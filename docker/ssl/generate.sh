#!/bin/sh
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout localhost.key -out localhost.crt -config localhost.conf
sudo chmod 777 *.crt
sudo chmod 777 *.key
