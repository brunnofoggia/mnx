'use strict';
/* foggia config */

const webpack = require('webpack');
const path = require('path');
const { merge } = require('webpack-merge');
const _ = require('underscore');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
// const IgnoreNotFoundExportPlugin = require('ignore-not-found-export-webpack-plugin');

// console.log('------------------------------', process?.env);
const lifecycle = process?.env?.npm_lifecycle_event || 'build'; // process.env.npm_lifecycle_event

const env = process?.env?.NODE_ENV || 'development';
const ISDEV =
    process?.env?.NODE_ENV === 'development' ||
    lifecycle.indexOf('start') !== -1 ||
    lifecycle.indexOf('dev') !== -1;

const target = process.env.TARGET || 'web';
const isCordova = target !== 'web';
const outputDir = isCordova ? 'cordova/www' : 'dist';
const publicPath = target === 'android' ? '/android_asset/www/' : '/';

console.log(env);
console.log(target);

function resolvePath(dir) {
    // if (isCordova) return path.join(__dirname, '..', dir); // codigo usado pelo framework7
    return path.join(__dirname, dir);
}

const webpackCommon = {
    mode: env,
    // target: env === 'development' ? 'web' : 'browserslist', // codigo usado pelo framework7
    target: target === 'web' ? 'web' : 'browserslist',
    experiments: {
        // asset: true,
    },
    entry: {
        app: ['./src/core/initialize'],
    },
    plugins: [
        // new IgnoreNotFoundExportPlugin(),
        new webpack.HotModuleReplacementPlugin({}),
        new CleanWebpackPlugin(),
        new webpack.ProvidePlugin({
            // all needed because of aes256
            crypto: require.resolve('crypto-browserify'),
            process: require.resolve('process/browser'),
            buffer: require.resolve('buffer'),
            stream: require.resolve('stream-browserify'),
            // assign buffer to Buffer
            Buffer: ['buffer', 'Buffer'],

            //            $: 'jquery',
            //            _: 'underscore',
            //            Bb: 'backbone',
            //            Mn: 'backbone.marionette',
        }),
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: './src/assets/rootfiles/',
                    to: './',
                },
                {
                    from: './src/core/cordova.js',
                    to: './',
                },
                {
                    from: './src/assets/porto-8.3.0/HTML/vendor',
                    to: './porto-vendor',
                },
                {
                    from: './src/assets/porto-8.3.0/HTML/js',
                    to: './porto-js',
                },
                {
                    from: './src/assets/porto-admin-3.1.0/HTML/vendor',
                    to: './porto-admin-vendor',
                },
                {
                    from: './src/assets/custom-porto-8.3.0',
                    to: './custom-porto',
                },
                // any image from this root folder will that need to be changed will have to receive a new name to break the cache
                {
                    from: './src/assets/pics',
                    to: './pics',
                },
                // {
                //     from: './src/assets/styles',
                //     to: './styles',
                // },
                {
                    from: './src/assets/fonts',
                    to: './fonts',
                    // flatten: true,
                },
                {
                    from: './src/assets/fonts',
                    to: './webfonts',
                    // flatten: true,
                },
                {
                    from: './src/assets/porto-8.3.0/HTML/img',
                    to: './img',
                },
                {
                    from: './src/public',
                    to: './public',
                },
            ],
        }),
        new HtmlWebpackPlugin({
            title: 'Navegador não suportado',
            meta: {
                robots: 'noindex, nofollow',
                viewport:
                    'width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no',
            },
            filename: 'navegador-suporte.html',
            template: './src/assets/html/navegador-suporte.jst',
            excludeChunks: ['app', 'vendor', 'runtime'],
        }),
        new HtmlWebpackPlugin({
            title: 'Navegador não suportado',
            meta: {
                robots: 'noindex, nofollow',
                viewport:
                    'width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no',
            },
            filename: 'navegador-suporte-mobile.html',
            template: './src/assets/html/navegador-suporte-mobile.jst',
            excludeChunks: ['app', 'vendor', 'runtime'],
        }),
        new HtmlWebpackPlugin({
            title: 'Navegador Info',
            meta: {
                robots: 'noindex, nofollow',
                viewport:
                    'width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no',
            },
            filename: 'navegador-info.html',
            template: './src/assets/html/navegador-info.jst',
            excludeChunks: ['app', 'vendor', 'runtime'],
        }),
        new webpack.DefinePlugin({
            ISDEV: ISDEV,
            ISPROD: !ISDEV,
            isCordova,
            platform: JSON.stringify(target),
            publicPath: JSON.stringify(publicPath),
            //            PRODUCTION: JSON.stringify(true),
            //            VERSION: JSON.stringify('5fa3b9'),
            //            BROWSER_SUPPORTS_HTML5: true,
            //            TWO: '1+1',
            //            'typeof window': JSON.stringify('object'),
            //            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
        }),
        new PreloadWebpackPlugin({
            // https://www.npmjs.com/package/preload-webpack-plugin
            rel: 'preload',
            include: 'allAssets',
            fileWhitelist: [
                /^\w+\.(png|svg|jpg|gif|woff|woff2|eot|ttf|otf|cur)$/,
            ],
            as(entry) {
                if (/\.css$/.test(entry)) return 'style';
                if (/\.(woff|woff2|eot|ttf|otf)$/.test(entry)) return 'font';
                if (/\.(png|svg|jpg|gif)/.test(entry)) return 'image';
                return 'script';
            },
        }),
    ],
    module: {
        rules: [
            // js
            {
                test: /\.m?js$/,
                resolve: {
                    // solves the need of completing file names with extension
                    fullySpecified: false, // disable the behaviour
                    // solves the problem of importing internal files inside modules of npm libs
                    symlinks: true,
                    fallback: {
                        // all needed because of aes256
                        crypto: require.resolve('crypto-browserify'),
                        buffer: require.resolve('buffer'),
                        process: require.resolve('process/browser'),
                        stream: require.resolve('stream-browserify'),
                        // all needed because of ejs
                        fs: false,
                        path: false,
                        os: false,
                    },
                },
            },
            // jst
            {
                test: /\.jst$/,
                loader: 'underscore-template-loader',
                options: {
                    interpolate: '\\{\\{(.+?)\\}\\}',
                    evaluate: '\\{%([\\s\\S]+?)%\\}',
                    escape: '\\{-(.+?)-\\}',
                },
            },
            // ejs
            {
                test: /\.(ejs)$/,
                use: 'raw-loader',
            },
            // txt / html
            {
                test: /\.(txt|html)$/i,
                exclude: /node_modules/,
                use: 'raw-loader',
            },
            // css
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                        options: {
                            injectType: 'linkTag',
                            attributes: {
                                // rel: 'stylesheet',
                                type: 'text/css',
                                // preload
                                rel: 'preload',
                                onload: "this.rel='stylesheet';document.onloadFired=true;this.onload = '';",
                                as: 'style',
                            },
                        },
                    },
                    { loader: 'file-loader' },
                ].filter(Boolean),
                //                exclude: /node_modules/,
            },
            {
                test: /\.(s(a|c)ss)$/,
                use: [
                    {
                        loader: 'style-loader',
                        options: {
                            injectType: 'linkTag',
                            attributes: {
                                // rel: 'stylesheet',
                                type: 'text/css',
                                // preload
                                rel: 'preload',
                                onload: "this.rel='stylesheet';document.onloadFired=true;this.onload = '';",
                                as: 'style',
                            },
                        },
                    },
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[contenthash].css',
                        },
                    },
                    { loader: 'sass-loader' },
                ].filter(Boolean),
                //                exclude: /node_modules/,
            },
            // img
            {
                test: /\.(png|svg|jpg|gif)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'file-loader',
                        // to solve img tag receiving esmodule instead of url / base64
                        options: {
                            esModule: false,
                        },
                    },
                ],
                // to stop Asset Module from processing your assets again as that would result in asset duplication
                type: 'javascript/auto',
            },
            // assets
            {
                test: /\.(woff|woff2|eot|ttf|otf|cur)$/,
                exclude: /node_modules/,
                use: ['file-loader'],
            },
        ],
    },
    output: {
        path: resolvePath(outputDir), // para onde vao os arquivos da build
        publicPath,
        filename: 'js/[name].[contenthash].js',
        // chunkFilename: 'js/[name].[contenthash].js',
        // hotUpdateChunkFilename: 'hot/hot-update.js',
        // hotUpdateMainFilename: 'hot/hot-update.json',
    },
    optimization: {
        runtimeChunk: 'single',
        moduleIds: 'deterministic',
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all',
                },
                styles: {
                    name: 'styles',
                    test: /\.css$/,
                    chunks: 'all',
                    enforce: true,
                },
            },
        },
    },
};

/* <metatags */
var index = new HtmlWebpackPlugin({
    title: 'my page',
    meta: {
        robots: 'nofollow,noindex',
        viewport:
            'width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no',
        keywords: 'palavras,chave',
        description: 'descricao',
    },
    filename: 'index.html',
    template: './src/assets/html/index.jst',
    templateParameters: {
        metaprops: {
            'og:description': 'descricao',
        },
    },
});

webpackCommon.plugins.push(index);
/* metatags> */

var webpackConfig;
switch (ISDEV) {
    case true:
        webpackConfig = merge(webpackCommon, {
            // mode: 'development',
            devtool: 'inline-source-map',
            devServer: {
                hot: true,
                https: true,
                http2: true, // require https
                compress: true,
                // progress: true,// deprecated com web-dev 4
                // writeToDisk: true,// deprecated com web-dev 4
                open: {
                    app: {
                        name: 'google-chrome',
                        // '--incognito',
                        // arguments: '--allow-insecure-localhost',
                    },
                },

                // inline: true,// deprecated com web-dev 4
                // contentBase: path.join(__dirname, outputDir), //path.resolve(__dirname, ISDEV ? 'public' : outputDir),// deprecated com web-dev 4
                // publicPath: '/',
                // disableHostCheck: true,// deprecated com web-dev 4
                historyApiFallback: true,
                //    historyApiFallback: {
                //        index: 'pvs.html',
                //     //    rewrites: [
                //     //        {from: /.*/, to: '/pvs.html'}
                //     //    ]
                //    }
            },
        });
        break;
    default:
        webpackConfig = merge(webpackCommon, {
            optimization: {
                minimize: true,
                minimizer: [new TerserPlugin()],
            },
            //            devtool: 'source-map',
        });
        break;
}

module.exports = webpackConfig;
