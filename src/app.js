/* eslint-disable no-undef */
/* App */
import { App, vx, mnx, ux, _, Bb } from './core/main';
import Auth from './models/auth';
import AppView from './views/app';

// libs
import fingerprint from './libs/fingerprint';
import geolocation from './libs/geolocation';

// data

var appdiv = document.createElement('div');
appdiv.id = 'app';
appdiv.classList.add('body');
document.body.appendChild(appdiv);

export default mnx.appSync
    .extend(
        _.extend(
            {},
            {
                logs: true,
                region: '#app',
                homeLink: '/',
                global: {},
                initialize(o) {
                    this.Auth = Auth;

                    this.options.AppView = AppView;
                    this.getFingerprint();

                    // related lists

                    _.bind(mnx.appSync.prototype.initialize, this)(o);
                },
                homeLoggedLink(tipoUsuario) {
                    var home = {};
                    home[usuarioTipo.CLIENTE] = '/painel/pedidos';
                    home[usuarioTipo.ATENDENTE] = '/painel/atendente';
                    // home[usuarioTipo.PARCEIRO] = '/painel/parceiro';
                    home[usuarioTipo.PARCEIRO] = '/painel/pedidos';

                    !tipoUsuario &&
                        (tipoUsuario = this.auth.usuario.get('tipoUsuario'));

                    return (this.auth.loaded() && home[tipoUsuario]) || '/';
                },
                fluxoLink() {
                    // 0 => boletos, 1 => cadastro
                    var link = this.auth.isLogged() ? 'boletos' : 'cadastro';
                    return link;
                },
                triggerReady() {
                    if (
                        _.bind(mnx.appSync.prototype.triggerReady, this)() ===
                        true
                    ) {
                        this.captureFingerprint();
                        //id de testes UA-189334193-1
                        this.loadGAnalytics(
                            vx.environment() === 'UA-189334193-1',
                            vx.environment() === null,
                            vx.environment() === null
                        );
                        // this.loadPixel();

                        if (vx.environment() === vx.constants.env.production)
                            setInterval(
                                () =>
                                    this.newWebpackBuildDetect(
                                        this.newWebpackBuildInfo
                                    ),
                                10000
                            );
                    }
                },
                headerStickyOptions() {
                    return JSON.parse(
                        $('header')
                            .attr('data-plugin-options')
                            .replace(/\'/g, '"')
                    );
                },
                headerHeight() {
                    var sticky = this.headerStickyOptions();
                    return parseInt(sticky.stickyHeaderContainerHeight, 10);
                },
                valorMin() {
                    return this.relatedLists.restricao.valorMin();
                },
                isLoggedEvents() {
                    this.appView.isLoggedEvents();
                },
                logout() {
                    this.auth.logout();
                },
                newWebpackBuildInfo() {
                    var refresh = () => {
                            window.location.reload();
                        },
                        $modal = Sk.popup.simple.info({
                            title: 'Atualização encontrada',
                            msg: 'Foi encontrada uma atualização do sistema. A página irá atualizar em 3 segundos.',
                        });

                    $('.btn', $modal).on('click', () => refresh());
                    setTimeout(() => refresh(), 3000);
                },
                restricao() {
                    // if(!window.location.pathname || window.location.pathname==='/') return;

                    if (
                        this.relatedLists.restricao.get('redButton') + '' ==
                        '2'
                    ) {
                        this.online = false;
                        if (/offline/.test(window.location.pathname)) return;
                        window.location.href = '/offline';
                        return true;
                    }
                    return false;
                },
                loadGAnalytics(analytics_id, ads_id, grumft = false) {
                    if (
                        _.indexOf(
                            [vx.constants.env.production],
                            vx.environment()
                        ) < 0
                    )
                        return;

                    _.bind(mnx.appSync.prototype.loadGAnalytics, this)(
                        analytics_id,
                        ads_id
                    );

                    if (grumft) {
                        $(
                            '<script async src="//api.grumft.com/fp_gft/fpgft_adv.js"></script>'
                        ).appendTo('head');
                    }
                },
                loadTagManager(w, d, s, l, i) {
                    if (
                        _.indexOf(
                            [vx.constants.env.production],
                            vx.environment()
                        ) < 0
                    )
                        return;

                    _.bind(mnx.appSync.prototype.loadTagManager, this)(
                        w,
                        d,
                        s,
                        l,
                        i
                    );
                },
                loadPixel(pixel_id) {
                    if (
                        _.indexOf(
                            [vx.constants.env.production],
                            vx.environment()
                        ) < 0
                    )
                        return;

                    _.bind(mnx.appSync.prototype.loadPixel, this)(pixel_id);
                },
                fbq(a, b, c) {
                    // vx.debug.log('fbq');
                    // vx.debug.log([a, b, c]);

                    if (
                        _.indexOf(
                            [vx.constants.env.production],
                            vx.environment()
                        ) < 0
                    )
                        return;
                    mnx.appSync.prototype.fbq(a, b, c);
                },
                gtag(a, b, c) {
                    //id de testes UA-189334193-1
                    // vx.debug.log('gtag');
                    // vx.debug.log([a, b, c]);

                    if (
                        _.indexOf(
                            [vx.constants.env.production],
                            vx.environment()
                        ) < 0
                    )
                        return;
                    mnx.appSync.prototype.gtag(a, b, c);
                },
            },
            fingerprint,
            geolocation
        )
    )
    .Mark('default');
