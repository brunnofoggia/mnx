import Bb from 'backbone';
import Mn from 'backbone.marionette';
import _ from 'underscore';
import v8n from 'v8n';
import jQuery from 'jquery';
import 'jquery-mask-plugin';

//window.Bb = Bb;
//window.Mn = Mn;
//window._ = _;
//window.v8n = v8n;
window.$ = jQuery;
window.jQuery = $;

document.addEventListener(
    'deviceready',
    async () => {
        const { vx, mnx } = await import('../core/main');
        await import('../config/main');
        const app = (await import('../app')).default;

        var App = new app();
        App.Mark();
        App.start();
    },
    false
);
