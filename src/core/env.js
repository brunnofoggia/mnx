import { vx, mnx, ux, _, Bb } from '../core/main';

var detect = () => {
    var h = window.location.hostname;
    if (
        /localhost/.test(h) ||
        /127\.0\.0\.1/.test(h) ||
        /0\.0\.0\.0/.test(h) ||
        /192\.168\.\d+\.\d+/.test(h) ||
        ISDEV
    ) {
        return vx.constants.env.development;
    }

    var env = vx.constants.env.production;
    switch (true) {
        case /(\.|_)?(dev|development)\./.test(h):
            env = vx.constants.env.development;
            break;
        case /(\.|_)?(test|testing)\./.test(h):
            env = vx.constants.env.testing;
            break;
        case /(\.|_)?(stage|staging|homol|homolog)\./.test(h):
            env = vx.constants.env.staging;
            break;
    }

    return env;
};

vx.debug.environment = vx.environment = detect;

export default {
    detect,
};
