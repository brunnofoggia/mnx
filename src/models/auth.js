import { App, vx, mnx, ux, _, Bb, Mn, v8n } from '../core/main';
import usuarioModel from './usuario';
import usuarioTipo from '../enum/usuarioTipo';
import { text, status, statusPositivo } from '../enum/usuarioStatus';

export default vx.models.localStorage
    .extend({
        idAttribute: 'userId',
        urlRoot: vx.envAuthUrl('usuarios/login', true),
        storedAttributes: [
            'accessToken',
            'usuario',
            'sessionId',
            'autologin',
            'startedAt',
        ],
        logoutAttributes: [
            'accessToken',
            'usuario',
            'senha',
            'sessionId',
            'autologin',
        ],

        validations() {
            return {
                usuario: true,
                senha: true,
            };
        },
        format: {
            usuario: function (v, m) {
                return this.formatUser(v, m);
            },
            senha: function (v, m) {
                return this.formatPass(v, m);
            },
        },
        formatUser(v, m) {
            if (m) {
                v = v.toLowerCase().trim();

                /* remove mask conditionally */
                var isUserDocument = !/\D+/.test(v.replace(/[0-9\-\.]/g, ''));
                if (isUserDocument) {
                    v = v.replace(/[^0-9]/g, '');
                    // will be used later to get user data. needs to be clean
                    this.attributes.usuario = v;
                }
            }
            return v;
        },
        formatPass(v, m) {
            if (m) {
                if (!/^sha-/.test(v)) {
                    v = 'sha-' + vx.format(v, 'sha256transfer', 1);
                    this.attributes.senha = v;
                }
                v = v.substr(4);
            }
            return v;
        },
        autoLogin: true,
        initialize(o) {
            vx.models.localStorage.prototype.initialize.call(this, o);

            this.on('sync', () => this.setSession());
            this.on('error', (m, xhr, o) =>
                vx.router.showAjaxError(
                    xhr,
                    null,
                    null,
                    'Os dados de acesso não conferem. Verifique-os e tente novamente.'
                )
            );

            vx.utils.when(
                () => this.autoLoginKey() && App().isReady(),
                () => this.start()
            );
        },
        startedAt() {
            if (!this.get('startedAt')) {
                this.set('startedAt', window.location.pathname);
            }
        },
        start() {
            this.autoLogin = !this.isLogged();

            this.startedAt();
            this.isLogged() ? this.getUsuario() : this.readAutoLogin();
        },
        redirectToClientPartner() {
            var h = this.get('accessToken'),
                p = window.location.pathname,
                t = this.getAutoLoginQuery(),
                u = 'https://' + [h, p, t].join('/').replace(/\/\//g, '/');

            App().ux.popup.info({
                title: 'Informação',
                body: this.get('errorMessage') || '',
            });
            this.set('accessToken', '');

            setTimeout(() => (window.location.href = u), 3000);
        },
        redirectToParentPartnerLogged(path) {
            var h = App().relatedLists.parceiro.get('parceiro').host,
                p = path || window.location.pathname,
                t = this.getAutoLoginQuery(),
                u = 'https://' + [h, p, t].join('/').replace(/\/\//g, '/');

            window.open(u);
        },
        setSession() {
            this.set('userId', null);
            this.setAutoLogin();
            if (this.get('code') === 71) {
                this.redirectToClientPartner();
                this.disableAutoLogin();
            } else {
                this.getUsuario();
            }
        },
        disableAutoLogin() {
            this.autoLogin = false;
        },
        autoLoginKey() {
            if (!App() || !App().settings) return;
            return App().settings.crypt + new Date().format('ddMMyyyy');
        },
        readAutoLogin() {
            var token = getQueryVariable('t');

            if (!token) return this.disableAutoLogin();

            try {
                var j = vx.utils.decrypt(this.autoLoginKey(), token);

                if (!_.isJSON(j)) return this.disableAutoLogin();

                this.login(j.usuario, j.senha);
            } catch (e) {
                this.disableAutoLogin();
            }
        },
        login(usuario, senha, callback) {
            var c = () => {
                    this.callback = callback;
                    this.set('usuario', usuario);
                    this.set('senha', senha);
                    this.save();
                },
                e = () => {
                    this.disableAutoLogin();
                };

            App().requireGeolocation({
                callback: _.partial(App().checkGeolocation, [
                    (status, data, optional) => c(),
                    (status, data, optional) => e(),
                ]),
                uid: 'login',
                popup: false,
            });
        },
        setAutoLogin() {
            // set the autologin token for redirection if needed
            this.autoLogin = false;
            var d = _.pick(this.attributes, 'usuario', 'senha'),
                autologin = vx.utils.encrypt(this.autoLoginKey(), d);

            this.set('autologin', autologin);
        },
        getAutoLoginQuery() {
            return this.isLogged()
                ? '?t=' + encodeURIComponent(this.get('autologin'))
                : '';
        },
        getUsuario(callback) {
            callback = callback || this.callback;
            this.callback = null;

            if (this.isLogged() || !('usuario' in this)) {
                this.usuario = null;
                vx.utils.when(
                    () => {
                        return (
                            typeof App() === 'object' &&
                            App() &&
                            typeof App().auth === 'object' &&
                            this.isLogged()
                        );
                    },
                    () => {
                        this.usuario = new usuarioModel({
                            usuario: this.get('usuario'),
                        });
                        this.listenToOnce(
                            this.usuario,
                            'error',
                            (model, xhr) => {
                                vx.router.showAjaxError(xhr);
                                this.logout();
                                typeof callback === 'function' &&
                                    callback(false);
                            }
                        );
                        this.listenToOnce(this.usuario, 'sync', () => {
                            typeof callback === 'function' && callback(true);
                            this.trigger('logged');
                        });
                        this.usuario.fetch({ reset: true });
                    }
                );
            }
        },
        set(attr, val) {
            var set = vx.vmodel.prototype.set.call(this, attr, val);

            if (set && typeof attr === 'string') {
                this.setOnLocalStorage(attr, val);
            }

            return set;
        },
        loaded() {
            return (
                !this.autoLogin &&
                (!this.isLogged() ||
                    (typeof this.usuario === 'object' &&
                        this.usuario &&
                        this.usuario.isReady() === true))
            );
        },
        // infos
        isLogged() {
            return !!this.get('accessToken');
        },
        isEmailActivated() {
            return this.loaded() && this.usuario && this.usuario.get('email')
                ? !!this.usuario.get('emailAtivo')
                : undefined;
        },
        isCellphoneActivated() {
            return this.loaded() && this.usuario && this.usuario.get('celular')
                ? !!this.usuario.get('celularAtivo')
                : undefined;
        },
        isActivated() {
            return this.isEmailActivated() && this.isCellphoneActivated();
        },
        isActive() {
            return _.indexOf(statusPositivo(), this.usuario.get('status')) >= 0;
        },
        isActivateAndAproved() {
            return this.isActivated() && this.isActive();
        },
        logout(silent = false) {
            for (var x of this.logoutAttributes) {
                // limpa variaveis do storage
                this.set(x, null);
                // remove dados preenchidos
                this.attributes = _.omit(this.attributes, x);
            }

            delete this.usuario;

            !silent && this.trigger('logout');
        },
        getType() {
            return this.loaded() && this.usuario.get('tipoUsuario');
        },
        isParceiro() {
            return (
                this.loaded() &&
                this.usuario.get('tipoUsuario') === usuarioTipo.PARCEIRO
            );
        },
        isCliente() {
            return (
                this.loaded() &&
                this.usuario.get('tipoUsuario') === usuarioTipo.CLIENTE
            );
        },
        isTeam() {
            return this.isAtendente() || this.isAdmin();
        },
        isAtendente() {
            return (
                this.loaded() &&
                this.usuario.get('tipoUsuario') === usuarioTipo.ATENDENTE
            );
        },
        isAdmin() {
            return (
                this.loaded() &&
                this.usuario.get('tipoUsuario') === usuarioTipo.ADMIN
            );
        },
        save() {
            /*
            vx.utils.when(
                () => !!App().getFingerprint(),
                () => {
                    this.set('geolocation', App().getGeolocationData());
                    var f = App().getFingerprint();
                    this.set(
                        'fingerprintRequest[dados]',
                        JSON.stringify(f.components)
                    );
                    this.set('fingerprintRequest[fingerprint]', f.hash);
            */
            // _.bind(vx.model.prototype.save, this)();
            _.bind(vx.models.localStorage.prototype.save, this)();
            // }
            // );
        },
    })
    .Mark('auth');
