import { vx, mnx, ux, _, v8n } from '../core/main';

export default vx.vmodel
    .extend({
        //    url: '//dcdows.acsp.com.br/scaffolding/find/?t=test',
        url() {
            var table = 'tests';
            var tpl = _.template('crud/scaffolding/{{param}}?t={{table}}');
            return vx.envUrl(tpl({ table: table, param: this.id }), true);
        },
        validations() {
            return {
                age: [
                    [this.validator().greaterThan(17), 'Somente maiores de 18'],
                ],
                name: [
                    [this.validator().string().minLength(3), 'Informe o nome'],
                ],
            };
        },
    })
    .Mark('test');
