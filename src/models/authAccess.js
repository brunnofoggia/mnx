import { vx, mnx, ux, _ } from '../core/main';

export default vx.vmodel
    .extend({
        idAttribute: 'path',
        titleAttribute: 'path',
        urlRoot() {
            if (/^\//.test(this.id)) this.id = this.id.substr(1);
            return vx.envUrl('auth/authorize/', true) + this.id + '?_=';
        },
        isAuthorized() {
            return (
                this.isReady() === true && this.get('authorization') === true
            );
        },
    })
    .Mark('authAccess');
