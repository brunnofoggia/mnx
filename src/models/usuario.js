import { vx, mnx, ux, _, Bb, Mn, v8n } from '../core/main';

export default vx.vmodel
    .extend({
        urlRoot: vx.envAuthUrl('usuario/', true),
        //    urlRoot: vx.envUrl('crud/customer', true),
        idAttribute: 'usuario',
        getCellphone() {
            return this.get('celular')
                .replace(/[^0-9]/, '')
                .replace(/^(\d{2})(\d{5}|\d{4})(\d{4})$/, '($1) $2-$3');
        },
        getDoc() {
            return this.get('cliente').documento;
        },
    })
    .Mark('usuario');
