// https://www.html5rocks.com/pt/tutorials/getusermedia/intro/

import _ from 'underscore-99xp';

var _stream = null;
var _tpl = null;

var _photos_taken = {};

var $currentEl = null;
var $fullcontainer = null;
var $container = null;
var $refContainer = null;

var hasGetUserMedia = function () {

    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        return 1;
    }
    // Legacy code below: getUserMedia 
    else if (navigator.getUserMedia) { // Standard
        return 2;
    } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
        return 3;
    } else if (navigator.mozGetUserMedia) { // Mozilla-prefixed
        return 4;
    }

    return 0;

//    mine
//    return !!(navigator.mediaDevices &&
//            navigator.mediaDevices.getUserMedia);
//    other https://whatwebcando.today/photos.html
//    return !(!navigator.getUserMedia && !navigator.webkitGetUserMedia &&
//            !navigator.mozGetUserMedia && !navigator.msGetUserMedia);
}

var getUserMedia = function (constraints, success, error) {
    //https://davidwalsh.name/browser-camera
    // Get access to the camera!
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia(constraints).
                then(success).catch(error);
    }
    // Legacy code below: getUserMedia 
    else if (navigator.getUserMedia) { // Standard
        navigator.getUserMedia(constraints, success, error);
    } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia(constraints, success, error);
    } else if (navigator.mozGetUserMedia) { // Mozilla-prefixed
        navigator.mozGetUserMedia(constraints, success, error);
    }


//    return navigator.mediaDevices.getUserMedia(constraints);
};

var getDevices = function (callback, kind = 'videoinput') {
    navigator.mediaDevices.enumerateDevices().then((devicesFound) => {
        var devices = [];
        devicesFound.forEach(function (device) {
            if (kind && device.kind == kind) {
//                console.log('----------');
//                console.log(device.kind + ": " + device.label +
//                        " id = " + device.deviceId);
//                console.log(device);
                devices.push(device);
            }
        });
        
        callback(devices);
    })
}

const getStream = function (callback, constraints, skipCurrentDevice=false) {
    if (_stream) {
        _stream.getTracks().forEach(function (track) {
            track.stop();
        });
    }

    if (!hasGetUserMedia()) {
        typeof callback === 'function' && callback(false, false);
        return;
    }

    !constraints && (constraints = {
        // vga
        video: true,
        // aspectRatio: {min: 1, max: 100},
        // width: 640,
        // height: 480,
//        width: {min: 1920},
//        video: {
//            width: {min: 640, ideal: 1280, max: 1920},
//            height: {min: 480, ideal: 720, max: 1080},
//        }
        // hd
//        video: {width: {min: 1280}, height: {min: 720}}
    });
    
    getDevices((devices) => {
        constraints = setWhichCamera(devices, constraints, skipCurrentDevice);
        getUserMedia(constraints, (stream) => gotStream(stream, callback), (error) => handleStreamError(error, callback));
    });
//setFocusMode
//FOCUS_MODE_AUTO
//FOCUS_MODE_INFINITY
//FOCUS_MODE_MACRO
//FOCUS_MODE_FIXED
//FOCUS_MODE_EDOF
//FOCUS_MODE_CONTINUOUS_VIDEO

//    getUserMedia(constraints, (stream) => gotStream(stream, callback), (error) => handleStreamError(error, callback));
}

var setWhichCamera = function(devices, constraints={}, skipCurrentDevice=false) {
    if(!$refContainer) return constraints;
    
    var currentDevice, $rc = $refContainer, camera = $rc.attr('data-camera-which')==='back' ? 'back' : 'face';
    
    if(devices[0].label != "" && devices.length > 1) {
        var i = 0;
        for(let device of devices) {
            var isFront = /(front|Webcam)/.test(device.label);
            if ((camera == 'back' && !isFront) || (camera == 'face' && isFront)) {
//                console.log('----------');
//                console.log(device.kind + ": " + device.label +
//                        " id = " + device.deviceId);
                
                if(skipCurrentDevice && devices.length > 1 && 
                    $refContainer.attr('data-camera-deviceid') && $refContainer.attr('data-camera-deviceid') === device.deviceId) {
                    if(!i) continue;
                    else break;
                }
                else {
                    currentDevice = device;
                    constraints.deviceId = { exact: device.deviceId };
                }
//                break; // let it get the last one to see if its better

                i++;
            }
        }
    } else {
        currentDevice = camera=='face' ? devices[0] : devices[devices.length-1];
        constraints.deviceId = { exact: currentDevice.deviceId };
    }
    
    $refContainer.attr('data-camera-deviceid', currentDevice.deviceId);
    
    constraints['video']===true && (constraints['video'] = {});
    constraints['video']['advanced'] = [{
        facingMode: camera=='face' ? 'user' : 'environment' // "user" for front
    }];
    
    
    return constraints;
}

var gotStream = function (stream, callback) {
    _stream = stream;
    typeof callback === 'function' && callback(true);
}

var handleStreamError = function (error, callback) {
    !_stream && typeof callback === 'function' && callback(false, error);
}

var getFullScreenContainer = function () {
    return $('#camera-shooter').length ? $('#camera-shooter') : null;
}

const getElements = function ($c) {
    !$c && ($c = $container);
    var $canvas = $('<canvas>'), $video = $('video', $c), $img = $('img', $c), $stream = $('.video-stream', $c);

    return [$stream, $video, $img, $canvas];
}

var deactivateAllVideo = function () {
    $('.camera-shooter-container').each((el, x) => {
        let $c = $(el);
        var [$stream, $video, $img, $canvas] = getElements($c);
        if (!$video.is(':visible'))
            return;

        $c.attr('data-camera-started', '0');
        deactivateVideo($video);
        $('.photo-sample', $c).eq(0).show();
    });

    $refContainer = null;
}

var setSrcObject = function (mediaControl, stream = null) {
    // https://whatwebcando.today/photos.html
    if ('srcObject' in mediaControl) {
        mediaControl.srcObject = stream;
    } else if (navigator.mozGetUserMedia) {
        mediaControl.mozSrcObject = stream;
    } else {
        mediaControl.src = (window.URL || window.webkitURL).createObjectURL(stream);
}

//    mediaControl.srcObject = stream;
}

var deactivateVideo = function ($video) {
    $video.hide();
    setSrcObject($video[0]);
}
var showVideo = function () {
    var [$stream, $video, $img, $canvas] = getElements();
    setSrcObject($video[0], _stream);
    $video.show();
}

const setTemplate = function (tpl) {
    _tpl = tpl;
}

var isStarted = function () {
    return $container && $refContainer && $refContainer.attr('data-camera-started') === '1';
}

var locateRefContainer = function (e) {
    var $fc = $(e.currentTarget).parents('#camera-shooter-container:first');
    if ($refContainer && $fc.is(':visible')) {
        return $refContainer;
    }

    var $rc = $(e.currentTarget).parents('.camera-shooter-container:first');
    return $rc;
}

var getCurrentContainer = function() {
    return isStarted() ? $refContainer : null;
}

var sameRefContainer = function (e) {
    var $rc = locateRefContainer(e);

    if (isStarted() && $refContainer.attr('data-camera-id') === $rc.attr('data-camera-id')) {
        return true;
    }
    return false;
}

var createContainer = function () {
    var $c = $(_tpl());

    $c.hide().appendTo('body');
    for (let e in events) {
        let c = events[e];
        let [ee, t, s] = e.match(/^(\w+)\s(.+)$/);

        // console.log([s, t].join(' -> '));
        $(s, $c).on(t, c);
    }

    return $c;
}

var shoot = function (e) {
    // console.log('shoot');
//    setTimeout(()=>{
    var [$stream, $video, $img, $canvas] = getElements();
    $canvas[0].width = $video[0].videoWidth;
    $canvas[0].height = $video[0].videoHeight;
    $canvas[0].getContext('2d').drawImage($video[0], 0, 0);
    // Other browsers will fall back to image/png

    storePhoto($canvas[0].toDataURL('image/jpeg'));
    $img.show()[0].src = getPhoto($refContainer);
    
    $('.camera-shooter img', $refContainer).trigger('change');
    deactivateVideo($video);
//    }, 5000);
}

var storePhoto = function (src) {
    _photos_taken[$refContainer.attr('data-camera-id')] = src;
}

const getPhoto = function ($rc) {
    return _photos_taken[$rc.attr('data-camera-id')];
}

const getPhotos = function () {
    return _photos_taken;
}

const clearPhoto = function ($rc) {
    try {
        if ($rc.attr('data-camera-id') in _photos_taken) {
            delete _photos_taken[$rc.attr('data-camera-id')];
        }
    } catch (e) {
    }
    ;
    return;
}

var shootAgain = function (e) {
    // console.log('shoot again');
    var [$stream, $video, $img, $canvas] = getElements();

    $img.hide().removeAttr('src');
    startShooting();
}

var startShooting = function (skipCurrentDevice=false) {
    var fn = () => {
        var [$stream, $video, $img, $canvas] = getElements();

        $('*:not(video)', $stream).hide();
        setSrcObject($video[0], _stream);
        $video.show();
    }
    
    getStream(fn, null, skipCurrentDevice);
}

var toggleShoot = function (e) {
    // console.log('toggle');
    if (!sameRefContainer(e)) {
        return takePhoto(e);
    }

    var [$stream, $video, $img, $canvas] = getElements();
    if ($video.is(':visible')) {
        return shoot(e);
    } else {
        return shootAgain(e);
    }
}

var finishShoot = function (e) {
    // console.log('finish');
    var [$stream, $video, $img, $canvas] = getElements();
    var [$streamRef, $videoRef, $imgRef, $canvasRef] = getElements($refContainer);

    // save photo
    $imgRef.show()[0].src = $img.show()[0].src;
    $('*', $streamRef).hide();
    !!$img.show()[0].src ? $('img', $streamRef).show() : $('.photo-sample', $streamRef).show();

    $refContainer.attr('data-camera-fullscreen') === '1' && $container.hide();

    deactivateAllVideo();
    $refContainer = null;
}

var takePhoto = function (e) {
    // console.log('take photo called');
    $currentEl = locateRefContainer(e);
    if (!$currentEl.length)
        return false;
    deactivateAllVideo();

    $refContainer = $currentEl;

    $refContainer.attr('data-camera-started', '1');
    !$refContainer.attr('data-camera-id') && $refContainer.attr('data-camera-id', _.uniqueId());

    if ($refContainer.attr('data-camera-fullscreen') === '1') {
        $fullcontainer = !getFullScreenContainer() ? createContainer() : getFullScreenContainer();
        $container = $fullcontainer;
    } else {
        $container = $refContainer;
    }

    var [$stream, $video, $img, $canvas] = getElements();

    // start shooting
    startShooting();

    $container.show();
    return true;
}

const events = Object.freeze({
    'click .camera-shooter .capture-start': (e) => takePhoto(e),
    'click .camera-shooter .capture-shoot': (e) => toggleShoot(e),
    'click .camera-shooter .capture-another': (e) => startShooting(true),
    'click .camera-shooter .capture-stop': (e) => finishShoot(e),
    'change .camera-shooter img': f=>f,
//    'click .camera-shooter .photo-took': '',
//    'click .camera-shooter .photo-sample': '',
});



const camera = Object.freeze({
    events: events,
    getStream: getStream,
    setTemplate: setTemplate,
    getPhotos: getPhotos,
    getPhoto: getPhoto,
    clearPhoto: clearPhoto,
    getCurrentContainer: getCurrentContainer,
});

export default camera;