import { App, vx, mnx, ux, _, Bb } from '../core/main';

export default {
    bots: {
        off: 'https://globalbot.ai/init/1/5F09F998C532134F5FD93D2A9EB801FA-2FCD-6A86-A848-E726691DF3AC',
        on: 'https://globalbot.ai/init/1/5F09F998C532134F5FD93D2A2DCC6488-0702-AACB-B089-78ED941F0CDC',
    },
    generateBotId() {
        var uid = _.uniqueId('bot');

        return uid;
    },
    getBotUrl() {
        var k = this.auth.isLogged() ? 'on' : 'off';
        return this.bots[k];
    },
    unloadBot() {
        $('iframe#gbt-frame').remove();
    },
    loadBot() {
        if (
            vx.environment() !== vx.constants.env.production ||
            (App().auth.isLogged() && !App().auth.isCliente())
        )
            return;

        vx.utils.when(
            () => this.appView.isTrullyVisible === true && vx.partner.id !== 0,
            () => {
                this.unloadBot();
                (function (a, b, c, d, e, f, g) {
                    a['botobject'] = e;
                    (a[e] =
                        a[e] ||
                        function () {
                            (a[e].q = a[e].q || []).push(arguments);
                        }),
                        (a[e].l = 1 * new Date());
                    (f = b.createElement(c)),
                        (g = b.getElementsByTagName('body')[0]);
                    f.async = 1;
                    f.src = d;
                    g.appendChild(f);
                })(
                    window,
                    document,
                    'script',
                    this.getBotUrl(),
                    this.generateBotId()
                );
            }
        );
    },
};
