import { App, vx, mnx, ux, _, Bb } from '../core/main';

export default {
    geolocation: function (uid) {
        // 0=>off, 1=>optional, 2=>mandatory
        return 0;
    },
    geolocationData: null,
    requireGeolocation({
        callback,
        popup = true,
        optional = null,
        uid = null,
    }) {
        var status = this.validateGeolocationData(
            this.getGeolocationData(),
            uid
        );

        typeof optional !== 'boolean' &&
            (optional = this.isGeolocationOptional(uid));

        if (status || optional)
            callback(status, this.getGeolocationData(), optional);
        if (status) return;

        !optional &&
            this.addLoading(
                'Por favor, permita o acesso à sua localização para continuar.',
                'geolocation'
            );
        vx.utils.geolocation((data) => {
            this.geolocationData = _.clone(data);
            status = this.validateGeolocationData(data, uid);
            this.removeLoading('geolocation');
            if (!status && !optional) {
                popup &&
                    ux.popup.info({
                        title: 'Informação',
                        body: `Para sua segurança, é necessário que você permita o acesso à sua localização para navegar em nosso site.
                            <br>
                            Para alterar as permissões de localização, entre nas configurações do navegador que está utilizando.`,
                    });
                this.navigate('/', { replace: true });
            } else if (!optional) {
                callback(status, data, optional);
            }
        });
    },
    validateGeolocationData(data, uid) {
        return !this.geolocation(uid) || (data && !('code' in data));
    },
    isGeolocationOptional(uid) {
        return this.geolocation(uid) !== 2;
    },
    getGeolocationData() {
        return _.clone(this.geolocationData);
    },
    checkGeolocation(c, status, data, optional) {
        var callback;
        if (status || optional) {
            if (!_.isArray(c)) callback = c;
            else callback = status ? c[0] : c[1];

            callback(status, data, optional);
        }
    },
};
