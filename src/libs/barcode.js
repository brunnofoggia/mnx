import Quagga from 'quagga';
import { vx, mnx, ux, _, Bb, Mn } from '../core/main';
import tpl from './barcode.jst';
import './barcode.css';
import './camera.css';

var Barcode = {
    template: tpl,
    events: {
        // 'click .camera-shooter .capture-shoot': 'tryAgain',
        'click .camera-shooter .capture-stop': 'finish',
    },
    start(close_callback, shoot_callback) {
        this.lastResult = null;
        this.setContainer();

        this.$container.show();
        setTimeout(() => {
            this.quagga_start();
            // this.setSrcObject($('video', this.$container)[0]);
        }, 500);

        this.close_callback = close_callback;
        this.shoot_callback = shoot_callback;
    },
    createContainer() {
        var $c = $(this.template());

        $c.hide().appendTo('body');
        for (let e in this.events) {
            let c = this.events[e];
            let [ee, t, s] = e.match(/^(\w+)\s(.+)$/);

            console.log([s, t].join(' -> '));
            $(s, $c).on(t, (e) => _.bind(this[c], this)(e));
        }

        return $c;
    },
    setContainer() {
        this.$container = !this.getContainer()
            ? this.createContainer()
            : this.getContainer();

        this.$container.css('height', $(window).height() + 'px');
        $('body')
            .css('height', $(window).height() + 'px')
            .css('overflow', 'hidden');
    },
    getContainer() {
        var $c = $('#camera-shooter').length ? $('#camera-shooter') : null;
        return $c;
    },
    getSrcObject(mediaControl) {
        var stream = null;
        if (mediaControl) {
            if ('srcObject' in mediaControl) {
                stream = mediaControl.srcObject;
            } else if (navigator.mozGetUserMedia) {
                stream = mediaControl.mozSrcObject = stream;
            } else {
                stream = mediaControl.src;
            }
        }

        return stream;
    },
    setSrcObject(mediaControl) {
        var stream = this.stream;

        if (stream) {
            // https://whatwebcando.today/photos.html
            if ('srcObject' in mediaControl) {
                mediaControl.srcObject = stream;
            } else if (navigator.mozGetUserMedia) {
                mediaControl.mozSrcObject = stream;
            } else {
                mediaControl.src = (
                    window.URL || window.webkitURL
                ).createObjectURL(stream);
            }
        }

        //    mediaControl.srcObject = stream;
    },
    // tryAgain() {
    //     this.lastResult = null;
    // },\
    close() {
        this.stream = this.getSrcObject($('video', this.$container)[0]);
        this.$container.remove();
        this.$container = null;
        $('body').css('height', 'initial').css('overflow', 'initial');
        Quagga.stop();
    },
    finish() {
        this.close();
        typeof this.close_callback === 'function' &&
            this.close_callback(this.lastResult);
    },
    setResult(n) {
        if (
            !typeof this.shoot_callback === 'function' ||
            this.shoot_callback(n)
        ) {
            this.lastResult = n;
            $('.result', this.$container).html(n);
        }
        // typeof this.shoot_callback === 'function' && this.shoot_callback(n);
    },
    quagga_start() {
        Quagga.init(
            {
                inputStream: {
                    name: 'Live',
                    type: 'LiveStream',
                    // target: document.querySelector('#firstquagga') // Or '#yourElement' (optional)

                    constraints: {
                        width: {
                            // min: 640 // camera ruim
                            min: 1280, // camera necessaria
                        },
                        height: {
                            // min: 480 // camera ruim
                            // min: 720 // camera necesaria
                        },
                        // aspectRatio: {
                        //     min: 1,
                        //     max: 100
                        // },
                        facingMode: 'environment', // or user
                    },
                },
                locator: {
                    // patchSize: "medium", // camera ruim
                    patchSize: 'small',
                    // halfSample: true
                },
                numOfWorkers: 4,
                decoder: {
                    // readers: ["ean_reader", "i2of5_reader"] //i2of5_reader
                    readers: ['i2of5_reader'], //i2of5_reader
                },
            },
            function (err) {
                if (err) {
                    console.log(err);
                    return;
                }
                // console.log("Initialization finished. Ready to start");
                Quagga.start();
            }
        );

        this.quagga_events();
    },
    quagga_events() {
        Quagga.onProcessed(function (result) {
            var drawingCtx = Quagga.canvas.ctx.overlay,
                drawingCanvas = Quagga.canvas.dom.overlay;

            if (result) {
                if (result.boxes) {
                    drawingCtx.clearRect(
                        0,
                        0,
                        parseInt(drawingCanvas.getAttribute('width')),
                        parseInt(drawingCanvas.getAttribute('height'))
                    );
                    result.boxes
                        .filter(function (box) {
                            return box !== result.box;
                        })
                        .forEach(function (box) {
                            vx.ENVDEV &&
                                Quagga.ImageDebug.drawPath(
                                    box,
                                    {
                                        x: 0,
                                        y: 1,
                                    },
                                    drawingCtx,
                                    {
                                        color: 'green',
                                        lineWidth: 2,
                                    }
                                );
                        });
                }

                if (result.box) {
                    vx.ENVDEV &&
                        Quagga.ImageDebug.drawPath(
                            result.box,
                            {
                                x: 0,
                                y: 1,
                            },
                            drawingCtx,
                            {
                                color: '#00F',
                                lineWidth: 2,
                            }
                        );
                }

                if (result.codeResult && result.codeResult.code) {
                    Quagga.ImageDebug.drawPath(
                        result.line,
                        {
                            x: 'x',
                            y: 'y',
                        },
                        drawingCtx,
                        {
                            color: 'red',
                            lineWidth: 3,
                        }
                    );
                }
            }
        });

        Quagga.onDetected(function (result) {
            var code = result.codeResult.code;

            // console.error(code);
            // if (!Barcode.lastResult) {
            if (Barcode.lastResult !== code) {
                switch (true) {
                    // case code.length === 13:
                    //     Barcode.setResult(code);
                    //     console.log('found 13');
                    //     break;
                    case code.length === 44:
                        Barcode.setResult(code);
                        // console.log('found 44');
                        break;
                }
                // var $node = null,
                //     canvas = Quagga.canvas.dom.image;

                // $node = $('<li><div class="thumbnail"><div class="imgWrBarcodeer"><img /></div><div class="caption"><h4 class="code"></h4></div></div></li>');
                // $node.find("img").attr("src", canvas.toDataURL());
                // $node.find("h4.code").html(code);
                // $("#result_strip ul.thumbnails").prepend($node);
            }
        });
    },
};

export default Barcode;
