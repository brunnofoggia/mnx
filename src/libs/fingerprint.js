import Fingerprint2 from 'fingerprintjs2';

export default {
    fingerprintHash: null,
    fingerprintComponents: null,
    captureFingerprint() {
        setTimeout(() => {
            Fingerprint2.get((components) => {
                this.fingerprintComponents = components;
                var values = components.map((component) => {
                    return component.value;
                });
                this.fingerprintHash = Fingerprint2.x64hash128(
                    values.join(''),
                    31
                );
            });
        }, 500);
    },
    getFingerprint() {
        return !this.fingerprintHash
            ? null
            : {
                  hash: this.fingerprintHash,
                  components: this.fingerprintComponents,
              };
    },
};
