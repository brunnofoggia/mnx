!(function (a, o) {
    'function' == typeof define && define.amd
        ? define(['underscore', 'jquery'], function (t) {
              return (a.Sk = o(a, t, $));
          })
        : (a.Sk = o(a, a._, a.$));
})(this, function (root, _, $) {
    'use strict';
    var previousInstance = root.Sk,
        Sk = {
            absolutePath: '/',
            imgPath: 'img/',
            applyAnimations: !0,
            AllowSpa: !1,
            events: {},
            VERSION: '1.3',
            noConflict: function () {
                return (root.Sk = previousInstance), this;
            },
            data: {
                popup: {
                    html: {
                        proxies: {
                            bootstrap4: {
                                close: '<a href="" class="btn btn-default modal-close">{close}</a>',
                                ok: '<a href="" class="btn btn-default modal-close">{ok}</a>',
                                div: `<div class="modal fade sk-popup" id="sk-popup-{id}" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="text-3 text-center w-100 title"></h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <div class="modal-body"></div>
                                                <div class="modal-footer"></div>
                                            </div>
                                        </div>
                                    </div>`,
                                img: '<img src="{img}"><br><br>{msg}',
                                html: '<div class="col-12 col-xs-12 clearfix">{html}</div>',
                                confirmMessage:
                                    '{msg}<textarea id="modal-confirm-message" rows="3" class="form-control">{val}</textarea>',
                                confirmActions:
                                    '<a href="" class="btn btn-sm confirm"></a><a href="" class="btn btn-sm cancel"></a>',
                            },
                        },
                    },
                    msg: {
                        imageViewTitle: 'Image View',
                        htmlViewTitle: 'HTML View',
                        infoViewTitle: 'Information',
                        confirmValidation: 'Field description is required',
                        confirmViewTitle: 'Confirmation',
                        confirmViewMsg: 'Are you sure about removing it?',
                        confirmActionMsg: 'Confirm',
                        cancelActionMsg: 'Cancel',
                        close: 'Close',
                        ok: 'Ok',
                    },
                },
            },
        };
    return (
        (Sk.popup = {
            init: function () {},
            events: {
                apply: function (a) {
                    var o = $(a || 'body');
                    o.delegate('.modal-link', 'click', function (a) {
                        Sk.preventDefault(a), Sk.popup.show($(this));
                    }),
                        $('.modal-image', o).addClass('hand-cursor'),
                        $('.modal-html', o).addClass('hand-cursor'),
                        o.delegate('.modal-image', 'click', function (a) {
                            var o = $(this).data('image');
                            Sk.preventDefault(a),
                                o &&
                                    Sk.popup.simple.image({
                                        img: Sk.absolutePath + o,
                                    });
                        }),
                        o.delegate('.modal-info', 'click', function (a) {
                            Sk.popup.simple.info($(this));
                        }),
                        o.delegate('.modal-html', 'click', function (a) {
                            var o = $(this).data('file');
                            Sk.preventDefault(a),
                                o
                                    ? $.get(Sk.absolutePath + o, function (a) {
                                          Sk.popup.simple.html({ html: a });
                                      })
                                    : Sk.popup.simple.html($(this));
                        }),
                        o.delegate('.popup-confirm', 'click', function (a) {
                            Sk.preventDefault(a),
                                Sk.popup.simple.confirm($(this));
                        });
                },
            },
            show: function (a) {
                'originalEvent' in a && (a = $(this));
                var o = Sk.popup;
                switch (a.data ? a.data('type') : a.type) {
                    case 'form':
                        o.form.show(a);
                        break;
                    default:
                        o.simple.info(a);
                }
            },
            form: {
                events: function (modal, obj) {
                    var self = Sk.popup,
                        proxy = self.proxies[self.proxy],
                        objData = obj.data ? obj.data() : obj,
                        footer = proxy.getSlice(modal, 'footer');
                    $('[type=submit]', footer).click(function (a) {
                        Sk.preventDefault(a),
                            $('form', $(this).parents('.modal:first')).submit();
                    }),
                        $('.modal-close', footer).click(function (a) {
                            Sk.preventDefault(a), Sk.popup.close(this, !1);
                        }),
                        objData.callback &&
                            ('string' == typeof objData.callback &&
                                (objData.callback = eval(objData.callback)),
                            'function' == typeof objData.callback &&
                                objData.callback(modal)),
                        proxy.clickBsModal(modal, function () {
                            Sk.popup.close($(this), !1);
                        }),
                        $('.datatable, .grid, .form, .view', modal).trigger(
                            'loaded'
                        );
                },
                show: function (a) {
                    var o,
                        t,
                        e = a,
                        s = {},
                        i = 'GET';
                    a.data && (e = a.data()),
                        (o = e.url || a.attr('href')),
                        (t = a.modalId
                            ? a.modalId
                            : o
                                  .replace(Sk.absolutePath, '')
                                  .replace(/\//g, '-')),
                        a.params && (o = o + '?' + a.params),
                        'sendData' in e && ((s = e.sendData), (i = 'POST')),
                        $.ajax({
                            url: o,
                            data: s,
                            type: i,
                            success: function (o) {
                                var e,
                                    s,
                                    i,
                                    l,
                                    n,
                                    p = Sk.popup,
                                    d = p.proxies[p.proxy];
                                (o = $(o)),
                                    (e = $(
                                        'header h3, header h4, header h5, .header h3, .header h4, .header h5',
                                        o
                                    ).eq(0)),
                                    (s = $(
                                        '.actions:not(.action-filters):last',
                                        o
                                    )),
                                    $('header, .header', o).eq(0).remove(),
                                    $(
                                        '.actions:not(.action-filters):last',
                                        o
                                    ).remove(),
                                    ((i = {}).id = t),
                                    (i.body = o),
                                    (i.header = 'header' in a ? a.header : e),
                                    (i.footer = 'footer' in a ? a.footer : s),
                                    (i.width = n =
                                        o.width()
                                            ? n
                                            : $(window).width() -
                                              d.sidesMargin),
                                    (l = Sk.popup.simple.show(i)),
                                    d.onShown(l),
                                    Sk.popup.form.events(l, a);
                            },
                            error: Sk.request.exception.ajaxError,
                        });
                },
            },
            simple: {
                events: function (a) {
                    $('.modal-close', a).click(function (a) {
                        Sk.preventDefault(a), Sk.popup.close(this, !1);
                    });
                },
                show: function (a) {
                    var o,
                        t,
                        e = Sk.popup,
                        s = e.proxies[e.proxy],
                        i = a.id || _.uniqueId('r'),
                        l = Sk.data.popup.html.proxies[e.proxy].div.replace(
                            '{id}',
                            i
                        );
                    for (o in ((t = $(l)), a))
                        -1 === $.inArray(o, ['id', 'width']) &&
                            (!1 === a[o]
                                ? s.getSlice(t, o).remove()
                                : s.getSlice(t, o).prepend(a[o]));
                    return (
                        a.width && s.setWidth(t, a.width),
                        s.show(t, a.sets || {}),
                        e.simple.events(t),
                        t
                    );
                },
                image: function (a) {
                    Sk.popup;
                    var o,
                        t = {},
                        e = a.data ? a.data() : a;
                    return (
                        (t.header =
                            'header' in e
                                ? e.header
                                : 'title' in e
                                ? e.title
                                : Sk.data.popup.msg.imageViewTitle),
                        (t.body = Sk.data.popup.html.proxies[Sk.popup.proxy].img
                            .replace('{img}', e.img)
                            .replace('{msg}', 'msg' in e ? e.msg : '')),
                        (t.footer = Sk.data.popup.html.proxies[
                            Sk.popup.proxy
                        ].close.replace('{close}', Sk.data.popup.msg.close)),
                        'id' in e && (t.id = e.id),
                        (o = Sk.popup.simple.show(t))
                            .addClass('info')
                            .addClass('image'),
                        o
                    );
                },
                html: function (a) {
                    var o,
                        t = {},
                        e = a;
                    return (
                        a.data && (e = a.data()),
                        (t.header =
                            'header' in e
                                ? e.header
                                : 'title' in e
                                ? e.title
                                : Sk.data.popup.msg.htmlViewTitle),
                        (t.body = Sk.data.popup.html.proxies[
                            Sk.popup.proxy
                        ].html.replace('{html}', e.html)),
                        (t.footer =
                            'footer' in e
                                ? e.footer
                                : Sk.data.popup.html.proxies[Sk.popup.proxy]
                                      .close),
                        'string' == typeof t.footer &&
                            t.footer.replace(
                                '{close}',
                                Sk.data.popup.msg.close
                            ),
                        'id' in e && (t.id = e.id),
                        (t.width = 'width' in e ? parseInt(e.width, 10) : 1e3),
                        (o = Sk.popup.simple.show(t))
                            .addClass('info')
                            .addClass('html'),
                        o
                    );
                },
                info: function (a) {
                    var o,
                        t = {},
                        e = a;
                    return (
                        a.data && (e = a.data()),
                        (t.header =
                            'header' in e && '' !== e.header
                                ? e.header
                                : 'title' in e && '' !== e.title
                                ? e.title
                                : Sk.data.popup.msg.infoViewTitle),
                        (t.body =
                            'body' in e && '' !== e.body
                                ? e.body
                                : 'msg' in e && '' !== e.msg
                                ? e.msg
                                : ''),
                        (t.footer = Sk.data.popup.html.proxies[
                            Sk.popup.proxy
                        ].ok.replace('{ok}', e.ok || Sk.data.popup.msg.ok)),
                        'id' in e && (t.id = e.id),
                        (t.sets = 'sets' in e ? e.sets : {}),
                        (o = Sk.popup.simple.show(t))
                            .addClass('info')
                            .addClass('message'),
                        o
                    );
                },
                confirm: function (obj) {
                    var data = {},
                        modal,
                        objData =
                            'function' == typeof obj.data ? obj.data() : obj;
                    return (
                        (data.width = objData.width || null),
                        (data.header =
                            objData.title ||
                            Sk.data.popup.msg.confirmViewTitle),
                        (data.body =
                            objData.msg || Sk.data.popup.msg.confirmViewMsg),
                        (data.footer =
                            Sk.data.popup.html.proxies[
                                Sk.popup.proxy
                            ].confirmActions),
                        (data.sets = 'sets' in objData ? objData.sets : {}),
                        (modal = Sk.popup.simple.show(data)),
                        (objData.modal = modal),
                        $('.modal-footer .confirm', modal)
                            .html(
                                objData.confirm ||
                                    Sk.data.popup.msg.confirmActionMsg
                            )
                            .addClass(
                                objData.confirmClass ||
                                    'btn-danger text-color-light'
                            ),
                        $('.modal-footer .cancel', modal)
                            .html(
                                objData.dataCancel ||
                                    Sk.data.popup.msg.cancelActionMsg
                            )
                            .addClass(objData.cancelClass || 'btn-default'),
                        $(
                            '.modal-footer .confirm, .modal-footer .cancel',
                            modal
                        ).click(
                            _.partial(function (objData, e) {
                                Sk.preventDefault(e);
                                var status = !0,
                                    url;
                                $(this).hasClass('cancel') && (status = !1),
                                    !$(this).hasClass('keepopened') &&
                                        Sk.popup.close($(this)),
                                    objData.callback ||
                                        ((url =
                                            obj.url ||
                                            (obj.data && obj.data('url'))
                                                ? obj.url || obj.data('url')
                                                : null),
                                        status && url && Sk.content.get(obj)),
                                    'string' == typeof objData.callback &&
                                        (objData.callback = eval(
                                            objData.callback
                                        )),
                                    'function' == typeof objData.callback &&
                                        objData.callback(status, objData);
                            }, objData)
                        ),
                        modal.addClass('confirm').addClass('info'),
                        modal
                    );
                },
                confirmMessage: function (obj) {
                    var data = {},
                        modal,
                        objData = obj;
                    return (
                        'function' == typeof obj.data && (objData = obj.data()),
                        (data.header =
                            objData.title ||
                            Sk.data.popup.msg.confirmViewTitle),
                        (data.bodyTpl =
                            objData.body ||
                            Sk.data.popup.html.proxies[Sk.popup.proxy]
                                .confirmMessage),
                        (data.body = data.bodyTpl
                            .replace(
                                '{msg}',
                                objData.msg || Sk.data.popup.msg.confirmViewMsg
                            )
                            .replace('{val}', '')),
                        (data.footer =
                            Sk.data.popup.html.proxies[
                                Sk.popup.proxy
                            ].confirmActions),
                        'width' in objData && (data.width = objData.width),
                        (modal = this.show(data)),
                        $('textarea', modal).val(objData.val),
                        $('.modal-footer .confirm', modal)
                            .html(
                                objData.confirm ||
                                    Sk.data.popup.msg.confirmActionMsg
                            )
                            .addClass(
                                objData.confirmClass ||
                                    'btn-danger text-color-light'
                            ),
                        $('.modal-footer .cancel', modal).html(
                            objData.dataCancel ||
                                Sk.data.popup.msg.cancelActionMsg
                        ),
                        $(
                            '.modal-footer .confirm, .modal-footer .cancel',
                            modal
                        ).click(
                            _.partial(function (modal, e) {
                                Sk.preventDefault(e);
                                var status = !0,
                                    $confirmMessage = $(
                                        '#modal-confirm-message'
                                    ),
                                    msg = $confirmMessage.val();
                                return (
                                    $(this).hasClass('cancel') && (status = !1),
                                    modal.attr('data-opt-msg') || msg || !status
                                        ? (Sk.popup.close($(this)),
                                          objData.callback
                                              ? ('string' ==
                                                    typeof objData.callback &&
                                                    (objData.callback = eval(
                                                        objData.callback
                                                    )),
                                                void (
                                                    'function' ==
                                                        typeof objData.callback &&
                                                    objData.callback(
                                                        msg,
                                                        status,
                                                        objData
                                                    )
                                                ))
                                              : Sk.popup.form.remove(
                                                    status,
                                                    obj
                                                ))
                                        : Sk.popup.simple.info({
                                              msg:
                                                  objData.confirmValidation ||
                                                  Sk.data.popup.msg
                                                      .confirmValidation,
                                          })
                                );
                            }, modal)
                        ),
                        modal.addClass('confirm').addClass('message'),
                        modal
                    );
                },
            },
            close: function (a) {
                var o,
                    t = Sk.popup;
                return (
                    (a = $(a || this)),
                    (o = t.proxies[t.proxy].getByReference(a)),
                    t.proxies[t.proxy].close(o)
                );
            },
        }),
        (Sk.popup.proxy = 'bootstrap4'),
        (Sk.popup.proxies = {}),
        (Sk.popup.proxies.bootstrap4 = {
            sidesMargin: 70,
            getByReference: function (a) {
                return a.hasClass('modal') ? a : a.parents('.modal');
            },
            close: function (a) {
                return (
                    a.removeClass('show'),
                    a.prev().filter('.modal-backdrop').removeClass('show'),
                    setTimeout(
                        _.partial(function (a) {
                            a.trigger('hidden.bs.modal');
                        }, a),
                        200
                    ),
                    !0
                );
            },
            getSlice: function (a, o) {
                var $e = $('.modal-' + o, a);
                o === 'header' && ($e = $('h4', $e));
                return $e;
            },
            setWidth: function (a, o) {
                _.indexOf(['lg', 'sm', 'full'], o) > -1
                    ? $('.modal-dialog', a).addClass('modal-' + o)
                    : (a
                          .css('width', o + this.sidesMargin)
                          .css('max-width', '100%')
                          .css('margin', 'auto'),
                      $('.modal-dialog', a).css('width', 'auto'));
            },
            show: function (a, o) {
                var t;
                for (t in o) a.attr('data-' + t, o[t]);
                a.modal('show'),
                    this.setZIndexes(a),
                    a
                        .addClass('show')
                        .on('hidden.bs.modal', function () {
                            $(this).prev().filter('.modal-backdrop').remove(),
                                $(this).remove(),
                                !$('body > .modal').length &&
                                    $('.modal-backdrop').remove();
                        })
                        .on('shown.bs.modal', function () {
                            $('.modal-footer .btn', $(this)).eq(0).focus();
                        });
            },
            setZIndexes: function (a) {
                if ($('.modal.show:last').length) {
                    var o = parseInt($('.modal.show:last').css('z-index'), 10);
                    a.css('z-index', o + 2),
                        $('.modal-backdrop:last').css('z-index', o + 1);
                }
            },
            clickBsModal: function (a, o) {
                a.on('hidden.bs.modal', o);
            },
            onShown: function (a) {
                a.on('shown.bs.modal', function () {
                    var o =
                        $(window).innerHeight() -
                        (parseInt(
                            $('.modal-dialog', a)
                                .css('margin-top')
                                .replace('px', ''),
                            10
                        ) +
                            parseInt(
                                $('.modal-dialog', a)
                                    .css('margin-bottom')
                                    .replace('px', ''),
                                10
                            ) +
                            parseInt(
                                $('.modal-header', a)
                                    .css('height')
                                    .replace('px', ''),
                                10
                            ) +
                            parseInt(
                                $('.modal-footer', a)
                                    .css('height')
                                    .replace('px', ''),
                                10
                            ));
                    $('.modal-body', a).css('max-height', o + 'px'),
                        Sk.events.apply(a);
                });
            },
        }),
        (Sk.preventDefault = function (a) {
            a && (a.preventDefault && a.preventDefault(), (a.returnValue = !1));
        }),
        (Sk.stopAllEvents = function (a) {
            a.stopPropagation(), this.preventDefault(a);
        }),
        (Sk.events.apply = function (a) {
            Sk.content.events.apply(a), Sk.popup.events.apply(a);
        }),
        Sk
    );
});
