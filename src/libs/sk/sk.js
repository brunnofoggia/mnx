// [deprecated]

(function (root, factory) {
    /* istanbul ignore next */
    if (typeof define === 'function' && define.amd) {
        define(['underscore', 'jquery'], function (_) {
            return (root.Sk = factory(root, _, $));
        });
    } else {
        root.Sk = factory(root, root._, root.$);
    }
})(this, function (root, _, $) {
    'use strict';

    // Baseline setup
    // --------------

    // Save the previous value of the `Sk` variable.
    var previousInstance = root.Sk;

    // Create a safe reference to the Skiver object for use below.
    var Sk = {
        absolutePath: '/',
        imgPath: 'img/',
        applyAnimations: true,
        AllowSpa: false,
        events: {},
    };

    Sk.VERSION = '1.3';

    // Run Skiver.js in *noConflict* mode, returning the `s` variable to its
    // previous owner. Returns a reference to the Skiver object.
    Sk.noConflict = function () {
        root.Sk = previousInstance;
        return this;
    };

    // A configurable set of messages, templates

    // Sample Data and Templates
    // -----------------
    Sk.data = {
        popup: {
            html: {
                proxies: {
                    bootstrap4: {
                        close: '<a href="" class="btn btn-default modal-close">{close}</a>',
                        ok: '<a href="" class="btn btn-default modal-close">{ok}</a>',
                        div: `<div class="modal fade sk-popup" id="sk-popup-{id}" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="text-3 text-center w-100 title"></h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>
                                        <div class="modal-body"></div>
                                        <div class="modal-footer"></div>
                                    </div>
                                </div>
                            </div>`,
                        img: '<img src="{img}"><br><br>{msg}',
                        html: '<div class="col-12 col-xs-12 clearfix">{html}</div>',
                        confirmMessage:
                            '{msg}<textarea id="modal-confirm-message" rows="3" class="form-control">{val}</textarea>',
                        confirmActions:
                            '<a href="" class="btn btn-sm confirm"></a><a href="" class="btn btn-sm cancel"></a>',
                    },
                },
            },
            msg: {
                imageViewTitle: 'Image View',
                htmlViewTitle: 'HTML View',
                infoViewTitle: 'Information',
                confirmValidation: 'Field description is required',
                confirmViewTitle: 'Confirmation',
                confirmViewMsg: 'Are you sure about removing it?',
                confirmActionMsg: 'Confirm',
                cancelActionMsg: 'Cancel',
                close: 'Close',
                ok: 'Ok',
            },
        },
    };

    /*
     * Utility for show content into popups
     */
    Sk.popup = {
        init: function () {},
        /**
         * Default useful events
         */
        events: {
            apply: function (parent) {
                var container = parent ? $(parent) : $('body');

                container.delegate('.modal-link', 'click', function (e) {
                    Sk.preventDefault(e);
                    Sk.popup.show($(this));
                });

                $('.modal-image', container).addClass('hand-cursor');
                $('.modal-html', container).addClass('hand-cursor');
                container.delegate('.modal-image', 'click', function (e) {
                    var img = $(this).data('image');
                    Sk.preventDefault(e);

                    if (img)
                        Sk.popup.simple.image({ img: Sk.absolutePath + img });
                });
                container.delegate('.modal-info', 'click', function (e) {
                    Sk.popup.simple.info($(this));
                });
                container.delegate('.modal-html', 'click', function (e) {
                    var file = $(this).data('file');
                    Sk.preventDefault(e);

                    if (file) {
                        $.get(Sk.absolutePath + file, function (response) {
                            Sk.popup.simple.html({ html: response });
                        });
                    } else {
                        Sk.popup.simple.html($(this));
                    }
                });
                container.delegate('.popup-confirm', 'click', function (e) {
                    Sk.preventDefault(e);
                    Sk.popup.simple.confirm($(this));
                });
            },
        },
        /**
         * Show popup according to data sent
         * @param jQuery obj any element or json data
         */
        show: function (obj) {
            'originalEvent' in obj ? (obj = $(this)) : null;
            var self = Sk.popup,
                type = obj.data ? obj.data('type') : obj.type;

            switch (type) {
                case 'form':
                    self.form.show(obj);
                    break;
                default:
                    self.simple.info(obj);
                    break;
            }
        },
        /**
         * Utilities to use Forms into Popup
         */
        form: {
            /**
             * Default events for forms into popup
             */
            events: function (modal, obj) {
                var self = Sk.popup,
                    proxy = self.proxies[self.proxy],
                    objData = obj.data ? obj.data() : obj,
                    footer = proxy.getSlice(modal, 'footer');

                $('[type=submit]', footer).click(function (e) {
                    Sk.preventDefault(e);
                    $('form', $(this).parents('.modal:first')).submit();
                });
                $('.modal-close', footer).click(function (e) {
                    Sk.preventDefault(e);
                    Sk.popup.close(this, false);
                });

                if (objData.callback) {
                    if (typeof objData.callback === 'string') {
                        objData.callback = eval(objData.callback);
                    }
                    if (typeof objData.callback === 'function') {
                        objData.callback(modal);
                    }
                }

                proxy.clickBsModal(modal, function () {
                    Sk.popup.close($(this), false);
                });

                $('.datatable, .grid, .form, .view', modal).trigger('loaded');
            },
            /**
             * Show a popup with form loaded
             * @param jQuery obj any element or json data with settings for set the popup
             */
            show: function (obj) {
                var objData = obj,
                    url,
                    sendData = {},
                    id,
                    params,
                    type = 'GET';
                if (obj.data) {
                    objData = obj.data();
                }

                url = objData.url || obj.attr('href');
                id = !obj.modalId
                    ? url.replace(Sk.absolutePath, '').replace(/\//g, '-')
                    : obj.modalId;
                if (obj.params) {
                    url = url + '?' + obj.params;
                }

                if ('sendData' in objData) {
                    sendData = objData.sendData;
                    type = 'POST';
                }

                // Sk.loading.modal.add();
                $.ajax({
                    url: url,
                    data: sendData,
                    type: type,
                    success: function (response) {
                        var self = Sk.popup,
                            proxy = self.proxies[self.proxy],
                            header,
                            actions,
                            data,
                            modal,
                            width;
                        response = $(response);

                        header = $(
                            'header h3, header h4, header h5, .header h3, .header h4, .header h5',
                            response
                        ).eq(0);
                        actions = $(
                            '.actions:not(.action-filters):last',
                            response
                        );

                        $('header, .header', response).eq(0).remove();
                        $(
                            '.actions:not(.action-filters):last',
                            response
                        ).remove();

                        data = {};
                        data.id = id;
                        data.body = response;

                        data.header = 'header' in obj ? obj.header : header;
                        data.footer = 'footer' in obj ? obj.footer : actions;

                        data.width = width = response.width()
                            ? width
                            : $(window).width() - proxy.sidesMargin;
                        modal = Sk.popup.simple.show(data);

                        proxy.onShown(modal);
                        /* events */
                        Sk.popup.form.events(modal, obj);
                        // Sk.loading.modal.remove();
                    },
                    error: Sk.request.exception.ajaxError,
                });
            },
        },
        /**
         * Utilities to show Content into Popup
         */
        simple: {
            /**
             * Default events for content into popup
             */
            events: function (modal) {
                $('.modal-close', modal).click(function (e) {
                    Sk.preventDefault(e);
                    Sk.popup.close(this, false);
                });
            },
            /**
             * Show content into a popup
             * @param jQuery obj any element or json data with settings for set the popup
             */
            show: function (data) {
                var self = Sk.popup,
                    proxy = self.proxies[self.proxy],
                    id = data.id || _.uniqueId('r'),
                    html = Sk.data.popup.html.proxies[self.proxy].div.replace(
                        '{id}',
                        id
                    ),
                    x,
                    modal;

                modal = $(html);

                for (x in data) {
                    if ($.inArray(x, ['id', 'width']) !== -1) {
                        continue;
                    }

                    if (data[x] === false) {
                        proxy.getSlice(modal, x).remove();
                    } else {
                        proxy.getSlice(modal, x).prepend(data[x]);
                    }
                }

                if (data.width) {
                    proxy.setWidth(modal, data.width);
                }

                proxy.show(modal, data.sets || {});
                self.simple.events(modal);

                return modal;
            },
            /**
             * Show image into a popup
             * @param jQuery obj any element or json data with settings for set the popup
             */
            image: function (obj) {
                var self = Sk.popup,
                    data = {},
                    objData = obj.data ? obj.data() : obj,
                    modal;

                data.header =
                    'header' in objData
                        ? objData.header
                        : 'title' in objData
                        ? objData.title
                        : Sk.data.popup.msg.imageViewTitle;
                data.body = Sk.data.popup.html.proxies[Sk.popup.proxy].img
                    .replace('{img}', objData.img)
                    .replace('{msg}', 'msg' in objData ? objData.msg : '');
                data.footer = Sk.data.popup.html.proxies[
                    Sk.popup.proxy
                ].close.replace('{close}', Sk.data.popup.msg.close);
                if ('id' in objData) {
                    data.id = objData.id;
                }

                modal = Sk.popup.simple.show(data);
                modal.addClass('info').addClass('image');

                return modal;
            },
            /**
             * Show html into a popup
             * @param jQuery obj any element or json data with settings for set the popup
             */
            html: function (obj) {
                var data = {},
                    objData = obj,
                    modal;
                if (obj.data) {
                    objData = obj.data();
                }

                data.header =
                    'header' in objData
                        ? objData.header
                        : 'title' in objData
                        ? objData.title
                        : Sk.data.popup.msg.htmlViewTitle;
                data.body = Sk.data.popup.html.proxies[
                    Sk.popup.proxy
                ].html.replace('{html}', objData.html);
                data.footer =
                    'footer' in objData
                        ? objData.footer
                        : Sk.data.popup.html.proxies[Sk.popup.proxy].close;
                typeof data.footer === 'string' &&
                    data.footer.replace('{close}', Sk.data.popup.msg.close);

                if ('id' in objData) {
                    data.id = objData.id;
                }

                data.width = !('width' in objData)
                    ? 1000
                    : parseInt(objData.width, 10);
                modal = Sk.popup.simple.show(data);

                modal.addClass('info').addClass('html');

                return modal;
            },
            /**
             * Show message into a popup
             * @param jQuery obj any element or json data with settings for set the popup
             */
            info: function (obj) {
                var data = {},
                    objData = obj,
                    modal;
                if (obj.data) {
                    objData = obj.data();
                }

                data.header =
                    'header' in objData && objData.header !== ''
                        ? objData.header
                        : 'title' in objData && objData.title !== ''
                        ? objData.title
                        : Sk.data.popup.msg.infoViewTitle;
                data.body =
                    'body' in objData && objData.body !== ''
                        ? objData.body
                        : 'msg' in objData && objData.msg !== ''
                        ? objData.msg
                        : '';
                data.footer = Sk.data.popup.html.proxies[
                    Sk.popup.proxy
                ].ok.replace('{ok}', objData.ok || Sk.data.popup.msg.ok);
                if ('id' in objData) {
                    data.id = objData.id;
                }
                data.sets = 'sets' in objData ? objData.sets : {};

                modal = Sk.popup.simple.show(data);
                modal.addClass('info').addClass('message');

                return modal;
            },
            /**
             * Show a confirmation popup with a message
             * @param jQuery obj any element or json data with settings for set the popup
             */
            confirm: function (obj) {
                var data = {},
                    modal,
                    objData = typeof obj.data === 'function' ? obj.data() : obj;

                data.width = objData.width || null;
                data.header =
                    objData.title || Sk.data.popup.msg.confirmViewTitle;
                data.body = objData.msg || Sk.data.popup.msg.confirmViewMsg;
                data.footer =
                    Sk.data.popup.html.proxies[Sk.popup.proxy].confirmActions;
                data.sets = 'sets' in objData ? objData.sets : {};

                modal = Sk.popup.simple.show(data);
                objData.modal = modal;
                $('.modal-footer .confirm', modal)
                    .html(objData.confirm || Sk.data.popup.msg.confirmActionMsg)
                    .addClass(
                        objData.confirmClass || 'btn-danger text-color-light'
                    );
                $('.modal-footer .cancel', modal)
                    .html(
                        objData.dataCancel || Sk.data.popup.msg.cancelActionMsg
                    )
                    .addClass(objData.cancelClass || 'btn-default');
                $('.modal-footer .confirm, .modal-footer .cancel', modal).click(
                    _.partial(function (objData, e) {
                        Sk.preventDefault(e);
                        var status = true,
                            url;
                        if ($(this).hasClass('cancel')) {
                            status = false;
                        }

                        !$(this).hasClass('keepopened') &&
                            Sk.popup.close($(this));
                        if (!objData.callback) {
                            url =
                                obj.url || (obj.data && obj.data('url'))
                                    ? obj.url || obj.data('url')
                                    : null;
                            if (status && url) {
                                Sk.content.get(obj);
                            }
                        }

                        if (typeof objData.callback === 'string') {
                            objData.callback = eval(objData.callback);
                        }
                        if (typeof objData.callback === 'function') {
                            objData.callback(status, objData);
                        }
                    }, objData)
                );

                modal.addClass('confirm').addClass('info');

                return modal;
            },
            /**
             * Show confirmation popup with a message and text field
             * @param jQuery obj any element or json data with settings for set the popup
             */
            confirmMessage: function (obj) {
                var data = {},
                    modal,
                    objData = obj;
                if (typeof obj.data === 'function') {
                    objData = obj.data();
                }

                data.header =
                    objData.title || Sk.data.popup.msg.confirmViewTitle;
                data.bodyTpl =
                    objData.body ||
                    Sk.data.popup.html.proxies[Sk.popup.proxy].confirmMessage;
                data.body = data.bodyTpl
                    .replace(
                        '{msg}',
                        objData.msg || Sk.data.popup.msg.confirmViewMsg
                    )
                    .replace('{val}', '');
                data.footer =
                    Sk.data.popup.html.proxies[Sk.popup.proxy].confirmActions;
                'width' in objData && (data.width = objData.width);

                modal = this.show(data);
                $('textarea', modal).val(objData.val);
                $('.modal-footer .confirm', modal)
                    .html(objData.confirm || Sk.data.popup.msg.confirmActionMsg)
                    .addClass(
                        objData.confirmClass || 'btn-danger text-color-light'
                    );
                $('.modal-footer .cancel', modal).html(
                    objData.dataCancel || Sk.data.popup.msg.cancelActionMsg
                );
                $('.modal-footer .confirm, .modal-footer .cancel', modal).click(
                    _.partial(function (modal, e) {
                        Sk.preventDefault(e);
                        var status = true,
                            $confirmMessage = $('#modal-confirm-message'),
                            msg = $confirmMessage.val();
                        if ($(this).hasClass('cancel')) {
                            status = false;
                        }

                        if (!modal.attr('data-opt-msg') && !msg && status) {
                            return Sk.popup.simple.info({
                                msg:
                                    objData.confirmValidation ||
                                    Sk.data.popup.msg.confirmValidation,
                            });
                        }

                        Sk.popup.close($(this));
                        if (!objData.callback) {
                            return Sk.popup.form.remove(status, obj);
                        }

                        if (typeof objData.callback === 'string') {
                            objData.callback = eval(objData.callback);
                        }
                        if (typeof objData.callback === 'function') {
                            objData.callback(msg, status, objData);
                        }
                    }, modal)
                );

                modal.addClass('confirm').addClass('message');

                return modal;
            },
        },
        /**
         * Close a popup
         * @param jQuery obj any element into the popup
         */
        close: function (obj) {
            var self = Sk.popup,
                modal;

            obj = !obj ? $(this) : $(obj);
            modal = self.proxies[self.proxy].getByReference(obj);

            return self.proxies[self.proxy].close(modal);
        },
    };

    Sk.popup.proxy = 'bootstrap4';
    Sk.popup.proxies = {};

    Sk.popup.proxies.bootstrap4 = {
        sidesMargin: 70,
        getByReference: function (reference) {
            return reference.hasClass('modal')
                ? reference
                : reference.parents('.modal');
        },
        close: function (modal) {
            modal.removeClass('show');
            modal.prev().filter('.modal-backdrop').removeClass('show');
            setTimeout(
                _.partial(function (modal) {
                    modal.trigger('hidden.bs.modal');
                }, modal),
                200
            );
            return true;
        },
        getSlice: function (modal, slice) {
            var $el = $('.modal-' + slice, modal);
            if (slice === 'header') {
                $el = $('h4', $el);
            }
            return $el;
        },
        setWidth: function (modal, width) {
            if (_.indexOf(['lg', 'sm', 'full'], width) > -1) {
                $('.modal-dialog', modal).addClass('modal-' + width);
                return;
            }
            modal
                .css('width', width + this.sidesMargin)
                .css('max-width', '100%')
                .css('margin', 'auto');

            $('.modal-dialog', modal).css('width', 'auto');
        },
        show: function (modal, sets) {
            var x;
            for (x in sets) {
                modal.attr('data-' + x, sets[x]);
            }

            modal.modal('show');
            this.setZIndexes(modal);

            modal
                .addClass('show')
                .on('hidden.bs.modal', function () {
                    var modal = this;
                    $(modal).prev().filter('.modal-backdrop').remove();
                    $(modal).remove();

                    !$('body > .modal').length && $('.modal-backdrop').remove(); // to ensure all backdrops were removed
                })
                .on('shown.bs.modal', function () {
                    //                        console.log(parseInt($(this).css('z-index'), 10)-1);
                    $('.modal-footer .btn', $(this)).eq(0).focus();
                });
        },
        setZIndexes: function (modal) {
            var lastModal = $('.modal.show:last');
            if (lastModal.length) {
                var currentZIndex = parseInt(
                    $('.modal.show:last').css('z-index'),
                    10
                );
                modal.css('z-index', currentZIndex + 2);
                $('.modal-backdrop:last').css('z-index', currentZIndex + 1);
            }
        },
        clickBsModal: function (modal, callback) {
            modal.on('hidden.bs.modal', callback);
        },
        onShown: function (modal) {
            modal.on('shown.bs.modal', function () {
                var bodyHeight =
                    $(window).innerHeight() -
                    (parseInt(
                        $('.modal-dialog', modal)
                            .css('margin-top')
                            .replace('px', ''),
                        10
                    ) +
                        parseInt(
                            $('.modal-dialog', modal)
                                .css('margin-bottom')
                                .replace('px', ''),
                            10
                        ) +
                        parseInt(
                            $('.modal-header', modal)
                                .css('height')
                                .replace('px', ''),
                            10
                        ) +
                        parseInt(
                            $('.modal-footer', modal)
                                .css('height')
                                .replace('px', ''),
                            10
                        ));
                $('.modal-body', modal).css('max-height', bodyHeight + 'px');

                Sk.events.apply(modal);
            });
        },
    };

    // Utility Functions
    // -----------------

    /**
     * Prevent default event
     * @param e event
     */
    Sk.preventDefault = function (e) {
        if (!e) return;

        if (e.preventDefault) e.preventDefault();
        e.returnValue = false;
    };

    /**
     * Prevent propagation
     * @param e event
     */
    Sk.stopAllEvents = function (e) {
        e.stopPropagation();
        this.preventDefault(e);
    };

    /**
     * Default useful events collection
     */
    Sk.events.apply = function (container) {
        Sk.content.events.apply(container);
        Sk.popup.events.apply(container);
    };

    return Sk;
});
