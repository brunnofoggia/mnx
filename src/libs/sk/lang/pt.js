import Sk from '../min.sk';

Sk.data.popup.msg.imageViewTitle = 'Visualização de Imagem';
Sk.data.popup.msg.htmlViewTitle = 'Visualização de HTML';
Sk.data.popup.msg.infoViewTitle = 'Informação';
Sk.data.popup.msg.confirmValidation = 'O campo de descrição é obrigatório';
Sk.data.popup.msg.confirmViewTitle = 'Confirmação';
Sk.data.popup.msg.confirmViewMsg = 'Deseja realmente excluir esse registro?';
Sk.data.popup.msg.confirmActionMsg = 'Confirmar';
Sk.data.popup.msg.cancelActionMsg = 'Cancelar';
Sk.data.popup.msg.close = 'Fechar';
