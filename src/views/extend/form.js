import { App, vx, mnx, ux, _ } from '../../core/main';
import global from './global';

export default mnx.views.form.extend(_.defaults({}, global));
