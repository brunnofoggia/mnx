export default {
    regions: {
        actions: '.actions',
    },
    actionsOptions: {
        addSpanForTitle: true,
        addClass: 'hidden-text-effect text-2',
    },
};
