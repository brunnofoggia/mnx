import { App, vx, mnx, ux, _ } from '../../core/main';
import global from './global';

export default mnx.views.grid.extend(
    _.defaults(
        {
            regions: _.extend(
                {
                    actions: '.actions',
                },
                mnx.views.grid.prototype.regions
            ),
            customize() {
                $('table', this.$el)
                    .addClass('table-hover')
                    .find('thead')
                    .addClass('bg-dark')
                    .find('th, th a')
                    .addClass('text-color-light');
            },
        },
        global
    )
);
