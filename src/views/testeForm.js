import { App, vx, mnx, ux, _ } from '../core/main';

import form from './extend/form';
import tpl from '../templates/testForm.ejs';
// just to make clear it works with both ejs and underscore
// import _tpl from '../templates/testForm.jst';
import Model from '../models/test';

export default form
    .extend({
        template: tpl,
        // template: _tpl,
        breadcrumbText: 'Teste - Form',
        Model,
    })
    .Mark('testeForm')
    .Shield({ authType: vx.router.authType.public });
