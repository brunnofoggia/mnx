import { App, vx, mnx, ux, _ } from '../core/main';
import tpl from '../templates/offline.ejs';

export default mnx.view
    .extend({
        template: tpl,
        initialize(opts) {
            App().appView.hideElements();

            vx.utils.when(
                () => $('*:first', this.$el).is(':visible'),
                () => this.scrollTop()
            );
        },
    })
    .Mark('offline');
