import { App, vx, mnx, ux, _ } from '../core/main';
// vendor CSS
//import 'bootstrap/dist/css/bootstrap.min.css';
import '../assets/porto-8.3.0/HTML/vendor/bootstrap/css/bootstrap.min.css';
import '../assets/custom-porto-8.3.0//vendor/fontawesome-free/css/all.min.css'; // 5.11.2
import '../assets/porto-8.3.0/HTML/vendor/animate/animate.min.css';
import '../assets/porto-8.3.0/HTML/vendor/simple-line-icons/css/simple-line-icons.min.css';
import '../assets/porto-8.3.0/HTML/vendor/owl.carousel/assets/owl.carousel.min.css';
import '../assets/porto-8.3.0/HTML/vendor/owl.carousel/assets/owl.theme.default.min.css';
//import '../assets/porto-8.3.0/HTML/vendor/magnific-popup/magnific-popup.min.css';
import '../assets/porto-8.3.0/HTML/css/theme.css';
import '../assets/porto-8.3.0/HTML/css/theme-elements.css';
//import '../assets/porto-8.3.0/HTML/css/theme-blog.css';
//import '../assets/porto-8.3.0/HTML/css/theme-shop.css';
import 'rangeslider-js/dist/styles.min.css';

// mandatory revolution slider
// import '../assets/porto-8.3.0/HTML/vendor/rs-plugin/revolution-addons/typewriter/css/typewriter.css';
import '../assets/porto-8.3.0/HTML/vendor/rs-plugin/css/settings.css';
import '../assets/porto-8.3.0/HTML/vendor/rs-plugin/css/layers.css';
import '../assets/porto-8.3.0/HTML/vendor/rs-plugin/css/navigation.css';
// import '../assets/porto-8.3.0/HTML/css/skins/skin-corporate-15.css';

import '../libs/sk/sk.css';
import '../styles/tables.css';
import '../styles/layout.css';
import '../styles/app.css';
import '../styles/bootstrap4-extension.css';

import '../styles/porto-skin.css';
import '../styles/colors.css';
import '../styles/custom.css';

// JS - vendor
import bootstrap from 'bootstrap/dist/js/bootstrap.bundle.min';
//import '../assets/porto-8.3.0/HTML/vendor/modernizr/modernizr.min.js';
// theme files
import theme from '../assets/custom-porto-8.3.0/js/theme.custom.full.js';
import themeInit from '../assets/custom-porto-8.3.0/js/theme.init.custom.full.js';

import template from '../templates/app.ejs';
import logo from '../assets/pics/vixs/logo_for_white.png';

import menu from '../templates/menu.ejs';
import old_assets from '../templates/old_assets.ejs';
import usuarioTipo from '../enum/usuarioTipo';

export default mnx.views.app
    .extend({
        template,
        templates: {
            menu,
        },
        data: {
            logo,
        },
        events: {
            'click .scroll-top': 'scrollTop',
            'click .logout': 'logout',
            'click .nav a[href^="/"]': 'closeMenu',
        },
        // metodo executado apos onstart do app
        initialize(o) {
            _.bind(mnx.views.app.prototype.initialize, this)(o);
            $('body').addClass('theme');
            // $('html').addClass(
            //     'side-header-hamburguer-sidebar side-header-hamburguer-sidebar-right side-header-hamburguer-sidebar-push side-header-hide side-header-right-no-reverse'
            // );
            this.options.vx = vx;
            this.options.old_assets = ejs.render(old_assets);
            this.waitToRender = true;
            vx.utils.when(
                () => {
                    vx.app().log(
                        'A-0 waiting App.settings. - ' +
                            typeof App().settings?.env
                    );
                    return (
                        typeof App().settings?.env !== 'undefined' &&
                        App().auth.loaded()
                    );
                },
                () => {
                    this.startAfterAttached();
                    this.waitToRender = false;
                }
            );
        },
        start() {
            vx.app().log('C-0 appview template set');
            this.setTemplate();

            _.bind(mnx.views.app.prototype.start, this)();
        },
        amIVisible() {
            return (
                $('footer').is(':visible') &&
                $('.main:first > .content > *:first').is(':visible')
            );
        },
        afterVisible() {
            $('.modal-wait').remove();
            _.bind(mnx.views.app.prototype.afterVisible, this)();
        },
        setTemplate() {
            $('body').attr('data-plugin-page-transition', '');
        },
        afterAppReadyRender() {
            $(window).on('scroll', () => this.closeMenu());
            _.bind(mnx.views.app.prototype.afterAppReadyRender, this)();
        },
        themeStart(forceInit = false, $el = null) {
            !$el && ($el = this.$el);
            vx.utils.when(
                () => {
                    return vx.app().allDone();
                },
                () => {
                    theme.init();
                    themeInit.set($el);
                    if (!this.$el.hasClass('init')) {
                        // init nav and sticky bar once
                        themeInit.exec(forceInit);
                        this.$el.addClass('init');
                    }
                    $('body').removeAttr('data-plugin-section-scroll');
                }
            );
        },
        setBreadcrumb(text) {
            $('.action-bar').show();

            $('.breadcrumb-title', this.$el).html(text);
        },
        isLoggedEvents() {
            // App().loadBot();
            if (App().auth.isLogged()) {
                $('.logged-content').show();
                $('.nologged-content').hide();
                const userType = App().auth.getType();
                if (userType) {
                    $('.' + userType + '-logged-content').show();
                }
            } else {
                $('.logged-content').hide();
                $('.nologged-content').show();
                for (var userType in _.invert(usuarioTipo)) {
                    $('.' + userType + '-logged-content').hide();
                }
            }
        },
        logout(e) {
            vx.events.stopAll(e);
            App().logout();
        },
        hideElements() {
            $('.header-nav, #mobile-menu-button, footer .footer-info').addClass(
                'd-none'
            );
            return this;
        },
        showElements() {
            $(
                '.header-nav, #mobile-menu-button, footer .footer-info'
            ).removeClass('d-none');
            return this;
        },
        colors: ['primary', 'secondary', 'tertiary', 'quartenary'],
        logos: {},
        headerColor: 'tertiary',
        defineLogos(c) {
            vx.utils.when(
                () =>
                    $('#header .logo-container:visible img.logo-img').length >
                    0,
                () => {
                    // if(!_.size(this.logos)) {
                    $('#header .logo-container:visible img.logo-img').each(
                        (x, el) => {
                            let $el = $(el);
                            this.logos[$el.attr('data-name')] = $el.attr('src');
                        }
                    );
                    // }
                    c();
                }
            );
        },
        closeMenu(e) {
            e && vx.events.preventDefault(e);
            $('.header-btn-collapse-nav').attr('aria-expanded') == 'true' &&
                $('.header-btn-collapse-nav').click();
        },
        afterReady() {
            App().appView.themeStart(true, this.$el);
            $('a.scroll-to-top:last').addClass('text-color-light');
        },
        afterNavigate() {
            // ensure header calculation for pages different than index
            _.bind(mnx.views.app.prototype.afterNavigate, this)();
        },
        getScrollTopPos() {
            return 0;
        },
    })
    .Mark('app');
