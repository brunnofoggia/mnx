import { App, vx, mnx, ux, _ } from '../core/main';
import grid from './extend/grid';

import collection from '../collections/test';

export default grid
    .extend({
        collection: new collection(),
        breadcrumbText: 'Teste - Lista',
        setGrid() {
            var opts = { collection: this.collection };
            opts.cols = [
                { name: 'id', title: 'Cód', align: 'right' },
                { name: 'name', title: 'Nome', align: 'right' },
                { name: 'age', title: 'Idade', align: 'right' },
            ];

            opts.filters = {};
            opts.filters.cols = [
                { name: 'name', title: 'Nome', type: 'text', filter: '%' },
            ];

            this.options = opts;
        },
    })
    .Mark('testeLista')
    .Shield({ authType: vx.router.authType.public });
