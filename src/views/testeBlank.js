import { App, vx, mnx, ux, _ } from '../core/main';
// just to make clear it works with both ejs and underscore
// import _tpl from '../templates/testForm.jst';

export default mnx.view
    .extend({
        template: '',
        breadcrumbText: 'Teste - Blank',
    })
    .Mark('testeBlank')
    .Shield({ authType: vx.router.authType.public });
