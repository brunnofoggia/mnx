import { App, vx, mnx, ux, _ } from '../core/main';
import template from '../templates/index.ejs';
import img from '../assets/pics/industria-4.0-2.jpg';

export default mnx.views.sync
    .extend({
        template,
        data: {
            img,
        },
        afterRender() {
            // ensure slider initialize properly
            App().appView.afterLoadOldAssets(() => {
                App().appView.themeStart(true, this.$el);
            });
        },
    })
    .Mark('index')
    .Shield({ authType: vx.router.authType.public });
