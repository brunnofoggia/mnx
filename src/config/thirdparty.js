import Bb from 'backbone';
import Mn from 'backbone.marionette';
import _ from 'underscore-99xp';
import ejs from 'ejs';
import v8n from 'v8n-99xp';

import 'front-99xp/src/extend';
import vx from 'backbone-front-99xp';
import mnx from 'marionette-99xp/src/marionette';
import toast from 'front-99xp/src/ux/bootstrap4/toast';
import loading from 'front-99xp/src/ux/loading';
// import loadingTpl from './templates/loading.jst';
import 'front-99xp/src/ux/loading.css';

import sk from '../libs/sk/min.sk';

// var vx = bbx;
var App = () => vx.locator.getItem('iApp');
window.Sk = sk;

loading.$el.find('i').attr('class', 'fas fa-cog fa-spin fa-3x p-2');
// loading.template = loadingTpl;
// loading.render();

vx.importView = function (v) {
    typeof v === 'string' && (v = { viewPath: v });
    const { viewName = '', viewPath = '' } = v;
    return new Promise((resolve, reject) => {
        import(`../views/${viewPath}.js`)
            .then((m) => {
                if (
                    m.default &&
                    (typeof m.default === 'function' || _.size(m.default) > 0)
                ) {
                    return resolve(m);
                }
                console.error('nao tem view (2)');
                window.location.href = '/';
                reject();
            })
            .catch((e) => {
                console.error('nao tem view (1)', e);
                window.location.href = '/';
                reject();
            });
    });
    // return import(`./views/${viewPath}.js`);
};

var ux = {
    toastDefaults: { positionY: 'top' },
    toast: _.defaults2(
        {
            add: (opts) => {
                return toast.add(_.defaults(opts, ux.toastDefaults));
            },
        },
        toast
    ),
    loading: loading,
    popup: {
        info(o) {
            return Sk.popup.simple.info(o);
        },
        confirm(o) {
            return Sk.popup.simple.confirm(o);
        },
        close($el) {
            Sk.popup.close($el);
        },
        confirmMessage(o) {
            return Sk.popup.simple.confirmMessage(o);
        },
    },
};
vx.ux = ux;

export { vx, mnx, sk, _, Bb, Mn, v8n, ux, App, ejs };
