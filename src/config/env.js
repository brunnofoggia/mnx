import { App, vx, mnx, ux, _, Bb } from '../core/main';
import env from '../core/env';
var schema = window.location.protocol.replace(/\W+/g, '');

vx.authUrl = {};
vx.authUrl[vx.constants.env.development] =
    schema + '://business.abpix.com.br/api/';
// vx.authUrl[vx.constants.env.development] = 'https://localhost:5000/api/';
vx.authUrl[vx.constants.env.testing] =
    schema + '://business.testing.abpix.com.br/api/';
vx.authUrl[vx.constants.env.staging] =
    schema + '://business.staging.abpix.com.br/api/'; // 84670000009448600800060130511254920058576314
vx.authUrl[vx.constants.env.production] =
    schema + '://business.abpix.com.br/api/';

vx.apiUrl = {};
vx.apiUrl[vx.constants.env.development] =
    schema + '://api.testing.abpix.com.br/';
vx.apiUrl[vx.constants.env.testing] = schema + '://api.testing.abpix.com.br/';
vx.apiUrl[vx.constants.env.staging] = schema + '://api.staging.abpix.com.br/'; // 84670000009448600800060130511254920058576314
vx.apiUrl[vx.constants.env.production] = schema + '://api.abpix.com.br/';

vx.wsUrl = 'https://abpix.vixs.com.br/ws/';
//vx.wsUrl = 'http://dlocalapp.com.br/ws/';

vx.formatUrl = function (baseUrl, url, useWsUrl = false) {
    useWsUrl && (url = baseUrl + url.replace(/^\//, ''));
    var scheme = url.match(/(\w+)\:\/\//);
    scheme = !scheme ? window.location.protocol.replace(/\W+/g, '') : scheme[1];

    url =
        scheme +
        '://' +
        ({ 0: '', 1: '', 3: '' }[vx.environment()] || '') +
        url.replace(scheme + '://', '');
    return url;
};

vx.envUrl = function (url, useWsUrl = false) {
    return vx.formatUrl(vx.wsUrl, url, useWsUrl);
};

vx.envAuthUrl = function (url, useWsUrl = false) {
    return vx.formatUrl(vx.authUrl[vx.environment()], url, useWsUrl);
};

vx.envApiUrl = function (url, useWsUrl = false) {
    return vx.formatUrl(vx.apiUrl[vx.environment()], url, useWsUrl);
};

export default {};
