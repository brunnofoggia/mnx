import { App, vx, mnx, ux, _, Bb, Mn } from '../core/main';

vx.router.Authorization = null;

Bb.ajax = function () {
    var addstartedAt = (xhr) => {
        // xhr.setRequestHeader('Origem', App().auth.get('startedAt'));
    };
    //    arguments[0].contentType = 'application/json';
    arguments[0].crossDomain = true;
    //    arguments[0].xhrFields = {withCredentials: true, cache: false};
    if (
        typeof App() === 'object' &&
        App() !== null &&
        typeof App().auth === 'object' &&
        App().auth !== null &&
        App().auth.isLogged()
    ) {
        arguments[0].beforeSend = (xhr) => {
            xhr.setRequestHeader(
                'Authorization',
                'Bearer ' + App().auth.get('accessToken')
            );
            addstartedAt(xhr);
        };
    } else {
        arguments[0].beforeSend = (xhr) => {
            addstartedAt(xhr);
        };
    }

    return Bb.$.ajax.apply(Backbone.$, arguments);
};

// shield
vx.router.setCheckViewAccess(function (viewName, id, routeData, callback) {
    vx.utils.when(
        () => {
            vx.app().log('E-0 waiting for auth to load');
            return (
                App() &&
                typeof App().auth === 'object' &&
                App().auth &&
                (!App().auth.isLogged() || App().auth.loaded())
            );
        },
        () => {
            vx.app().log('E-1 auth loaded');
            App().appView.afterNavigate();
            var subcallback = () => {
                // will garantee user activated it's account after registering
                return vx.router.checkView(viewName, id, routeData, callback);
            };

            if (
                'authType' in routeData &&
                routeData.authType !== vx.router.authType.public
            ) {
                if (!App().auth.isLogged()) {
                    var authRoute = _.result2(
                        routeData,
                        'authRoute',
                        vx.constants.auth.route.preferredAuth,
                        [viewName, id, routeData]
                    );

                    import(`../views/${authRoute}`).then(() => {
                        var loginView = new (vx.locator.getListItem(
                            'view',
                            authRoute
                        ))();
                        vx.utils.showView(loginView, null, subcallback);
                    });

                    return;
                } else if (!App().auth.isActivated()) {
                    import(`../views/${vx.constants.auth.route.activate}`).then(
                        () => {
                            var ativacaoView = new (vx.locator.getListItem(
                                'view',
                                vx.constants.auth.route.activate
                            ))();
                            vx.utils.showView(ativacaoView, null, subcallback);
                        }
                    );

                    return;
                }
                // help to keep different access for different type of users
                else if ('validate' in routeData && !routeData.validate()) {
                    vx.router.navigate('/', {
                        trigger: true,
                    });
                    App().ux.popup.info({
                        title: 'Ooops, temos um problema',
                        msg: 'Suas credenciais não permitem acesso a essa página',
                    });
                    return;
                }
            }
            return callback();
        },
        10
    );
});

vx.router.logout = function (success) {
    App().auth.logout();
};
