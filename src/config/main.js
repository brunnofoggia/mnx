import { App, vx, mnx, ux, _, Bb } from '../core/main';
import v8n from 'v8n-99xp';
import env from '../core/env';
import './env';
import form from 'marionette-99xp/src/views/form';
import validate from 'validate-99xp';
import '../libs/sk/lang/pt';

import './auth';

// aliases de rotas
// vx.router.routeViewPathAlias['aaa'] = 'xxx/yyy';

// platform
if (localStorage.getItem('platform')) {
    window.platform = localStorage.getItem('platform');
}
if (getQueryVariable('platform')) {
    window.platform = getQueryVariable('platform');
}
localStorage.setItem('platform', window.platform);

/* customizations */

// tratativa pra interpretar erros da api
vx.router.showAjaxError = function (
    jqXHR,
    textStatus,
    errorThrown,
    customMessage,
    showCallback
) {
    var msg,
        title,
        errorMsgDefault =
            'Estamos enfrentando instabilidade em nossos serviços. Tente novamente mais tarde.';
    if (jqXHR.responseJSON) {
        var json = jqXHR.responseJSON;
        msg =
            json['errorMessage'] ||
            json['message'] ||
            customMessage ||
            errorMsgDefault;
        switch (true) {
            case json.code === 0:
                msg = errorMsgDefault;
                break;
        }
        title = 'Ooops, temos um problema (' + (json.code || '999') + ')';
    } else {
        msg = errorMsgDefault;
        switch (true) {
            case jqXHR.status === 401:
                msg = 'Sessão expirada';
                break;
            case jqXHR.status === 500:
                msg = errorMsgDefault;
                break;
        }
        title = 'Informação';
        msg = customMessage ? customMessage : msg;
    }

    typeof showCallback === 'function'
        ? showCallback({ title, msg })
        : App().ux.popup.info({ title: title, msg: msg });

    switch (true) {
        case jqXHR.status === 401:
            App().auth.logout(); // listener do logout no app redireciona pra home
            break;
    }
};

// rotas que o modulo de autenticacao usa pra redirecionar o usuario
vx.constants.auth.route = {
    login: 'login',
    signup: 'cadastro',
    activate: 'ativacao',
    preferredAuth: 'login',
};

// mensagem padrao de erro de validacao de form
vx.vmodel.prototype.requiredErrorMessage = vx.validation.requiredErrorMessage =
    'Preenchimento obrigatório';

// padronizacao de titulo de popup
ux.showPopupInfo = function (o) {
    o = _.defaults(o, { title: 'Informação' });
    Sk.popup.simple.info(o);
};

// padronizacao do css do toast de sucesso
ux.showInfo = function (o) {
    o = _.defaults(o, {
        color: 'primary font-weight-bold text-light',
        autohide: true,
        delay: 5000,
        close: true,
        positionY: 'top',
    });
    App().ux.toast.add(o);
};

// padronizacao do css do toast de erro
ux.showError = function (o) {
    o = _.defaults(o, {
        color: 'danger font-weight-bold text-light',
        autohide: true,
        delay: 5000,
        close: true,
        positionY: 'top',
    });
    App().ux.toast.add(o);
    // o = _.defaults(o, { title: 'Ooops, temos um problema' });
    // ux.showInfo(o);
};

// tratativa pra falha no ajax que salva os dados de um form
mnx.views.form.prototype.showSyncError = form.prototype.showSyncError =
    function (model, xhr) {
        var json = {};
        if (xhr.responseJSON) {
            json = xhr.responseJSON;
        } else {
            try {
                json = JSON.parse(xhr.responseText);
            } catch (e) {}
        }

        var msg = 'Falha interna ao tentar salvar o registro';
        if (json && (json['errorMessage'] || json['message'])) {
            msg = json['errorMessage'] || json['message'];
        } else if ('authorization' in json && json.authorization)
            msg = 'Acesso negado';

        ux.showError({ msg: msg, color: 'danger text-dark font-weight-bold' });
    };

// app settings
vx.utils.when(
    () => App() && vx.envAuthUrl,
    () => {
        App().ux = {};
        App().ux = ux;

        App().settings = {};
        App().settings.crypt = 'iS}Q%)cXjgS3)r,.';
        App().settings.env = vx.environment();
    },
    1
);

export default {};
