import { vx, mnx, ux, _ } from '../core/main';
import model from '../models/authAccess';

export default vx.collection
    .extend({
        initialize(models = [], items = []) {
            if (typeof items === 'object')
                for (let item of items) {
                    let imodel = new model(item);
                    this.addRelatedList(item.path, imodel, true);
                }
        },
        fetch() {
            this.fetchRelatedLists();
            this.trigger('sync');
        },
    })
    .Mark('authAccess');
