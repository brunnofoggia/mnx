import { vx, mnx, ux, _ } from '../core/main';
import model from '../models/test';

export default vx.collections.grid
    .extend({
        url: vx.envUrl('crud/scaffolding/?t=tests', true),
        model,
    })
    .Mark('test');
