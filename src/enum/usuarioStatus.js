import { App, vx, mnx, ux, _, Bb, Mn, v8n } from '../core/main';

var text = {
    I: 'Inativo',
    A: 'Ativo',
    B: 'Bloqueado', // excesso de tentativas de login
};

var status = _.invert(text);

var statusPositivo = function () {
    return [status.Ativo];
};

export { text, status, statusPositivo };
export default Object.freeze(text);
