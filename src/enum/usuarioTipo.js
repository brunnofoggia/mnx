export default Object.freeze({
    'CLIENTE': 'C',
    'ATENDENTE': 'A',
    'PARCEIRO': 'P',
    'ADMIN': '0'
});